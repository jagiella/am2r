#
# A more complicated makefile
#
UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
echo 'Linux'
DEVKITARM := /home/nick/Downloads/devkitARM
TONCLIB   := /home/nick/Code/code/tonclib
endif
ifeq ($(UNAME), Darwin)
DEVKITARM := /Users/jagiella/Downloads/devkitARM
TONCLIB   := /Users/jagiella/Downloads/code/tonclib
endif

PATH := $(DEVKITARM)/bin:$(PATH)

# --- Project details -------------------------------------------------

PROJ    := am2r
TARGET  := $(PROJ)

# C++
OBJP    := am2r.o Tilemap.o SpriteEngine.o ObjectHandler.o BackgroundHandler.o Collision.o Samus.o Tsumuri.o Blocks.o HUD.o
# C
OBJS    := sprites/samus/gripping.o sprites/samus/morphball.o sprites/samus/spinjump.o sprites/samus/jumping.o sprites/samus/samus-running-sheet.o sprites/samus/samus-standing-sheet.o
OBJS    += sprites/gunship.o
OBJS    += sprites/savepoint.o
OBJS    += sprites/metroids/alpha.o
OBJS    += sprites/hud/numbersLarge.o
OBJS    += sprites/beam.o sprites/tsumuri/tsumuri.o sprites/pupu.o sprites/horonad/horonad-standing.o tilesets/blocks.o   
OBJS    += $(TONCLIB)/src/tonc_oam.o $(TONCLIB)/src/tonc_input.o $(TONCLIB)/src/tonc_core.o
OBJS    += tilesets/collisionTiles.o 
OBJS    += tilemaps/background1.o tilemaps/background2.o 
OBJS    += tilemaps/surface1.o tilemaps/surface1-0.o tilemaps/surface1-1.o 
OBJS    += tilemaps/surface2.o tilemaps/surface2-0.o tilemaps/surface2-1.o 
OBJS    += tilemaps/surface4.o tilemaps/surface4-0.o tilemaps/surface4-1.o
OBJS    += tilemaps/surface5.o tilemaps/surface5-0.o tilemaps/surface5-1.o
OBJS    += tilemaps/surface6.o tilemaps/surface6-0.o tilemaps/surface6-1.o
OBJS    += tilemaps/surface7.o tilemaps/surface7-0.o tilemaps/surface7-1.o
OBJS    += tilemaps/surface8.o tilemaps/surface8-0.o tilemaps/surface8-1.o
OBJS    += tilemaps/surface9.o tilemaps/surface9-0.o tilemaps/surface9-1.o
OBJS    += tilemaps/surface10.o tilemaps/surface10-0.o tilemaps/surface10-1.o
OBJS    += tilemaps/surface11.o tilemaps/surface11-0.o tilemaps/surface11-1.o
# ASM
SOBJ    := $(TONCLIB)/asm/tonc_memcpy.o $(TONCLIB)/asm/tonc_bios.o $(TONCLIB)/asm/tonc_nocash.o 

# --- Build defines ---------------------------------------------------

PREFIX  := $(DEVKITARM)/bin/arm-none-eabi-
CPP     := $(PREFIX)g++
CC      := $(PREFIX)gcc
LD      := $(PREFIX)g++
OBJCOPY := $(PREFIX)objcopy
AS      := $(PREFIX)as

ARCH    := -mthumb-interwork -mthumb
SPECS   := -specs=gba.specs

CFLAGS  := $(ARCH) -O5 -Wall -fno-strict-aliasing -I$(TONCLIB)/include -Wno-narrowing
LDFLAGS := $(ARCH) $(SPECS)
ASFLAGS  := $(ARCH) -I$(TONCLIB)/include

.PHONY : build clean

# --- Build -----------------------------------------------------------
# Build process starts here!
all: $(TARGET).gba
	vba -4 $(TARGET).gba 

# Strip and fix header (step 3,4)
$(TARGET).gba : $(TARGET).elf
	$(OBJCOPY) -v -O binary $< $@
	#-@gbafix $@

# Link (step 2)
$(TARGET).elf : $(OBJS) $(SOBJ) $(OBJP)
	$(CPP) -x assembler-with-cpp -mthumb-interwork -mthumb -c gba_crt0.s -o gba_crt0.o
	$(LD) $^ $(LDFLAGS) -o $@

# Compile (step 1)
$(OBJP) : %.o : %.cpp
	$(CPP) -c $< $(CFLAGS) -o $@

$(OBJS) : %.o : %.c
	$(CC) -c $< $(CFLAGS) -o $@

$(SOBJ) : %.o : %.s
	#$(AS) -c $< $(AFLAGS) -o $@
	$(CC) -MMD -MP -MF $(DEPSDIR)/$*.d -x assembler-with-cpp $(ASFLAGS) -c $< -o $@
	
		
# --- Clean -----------------------------------------------------------

clean : 
	@rm -fv gba_crt0.o
	@rm -fv *.gba
	@rm -fv *.elf
	@rm -fv $(OBJP) $(OBJS) $(SOBJ)

#EOF