/* TODO:
 * 1. fake block implementation
 * 2. beam interaction with objects
 * 3. beam object destruction on contact or leaving map
 */






#include "Blocks.h"
#include "sprites/beam.h"
#include "sprites/pupu.h"
#include "sprites/gunship.h"
#include "sprites/savepoint.h"
#include "sprites/hud/numbersLarge.h"

#include "tilemaps/background1.h"
#include "tilemaps/background2.h"
#include "tilemaps/surface1.h"
#include "tilemaps/surface1-0.h"
#include "tilemaps/surface1-1.h"
#include "tilemaps/surface2.h"
#include "tilemaps/surface2-0.h"
#include "tilemaps/surface2-1.h"
//#include "tilemaps/surface3.h"
//#include "tilemaps/surface3-0.h"
//#include "tilemaps/surface3-1.h"
#include "tilemaps/surface4.h"
#include "tilemaps/surface4-0.h"
#include "tilemaps/surface4-1.h"
#include "tilemaps/surface5.h"
#include "tilemaps/surface5-0.h"
#include "tilemaps/surface5-1.h"
#include "tilemaps/surface6.h"
#include "tilemaps/surface6-0.h"
#include "tilemaps/surface6-1.h"
#include "tilemaps/surface7.h"
#include "tilemaps/surface7-0.h"
#include "tilemaps/surface7-1.h"
#include "tilemaps/surface8.h"
#include "tilemaps/surface8-0.h"
#include "tilemaps/surface8-1.h"
#include "tilemaps/surface9.h"
#include "tilemaps/surface9-0.h"
#include "tilemaps/surface9-1.h"
#include "tilemaps/surface10.h"
#include "tilemaps/surface10-0.h"
#include "tilemaps/surface10-1.h"
#include "tilemaps/surface11.h"
#include "tilemaps/surface11-0.h"
#include "tilemaps/surface11-1.h"

//#include "enemies.h"
//#include "snd_01_AM2R_Title.h"
#include "sounds/snd_04_AM2R_Initial_Decent.h"

#include "SoundEngine.h"
#include "SpriteEngine.h"
#include "ObjectHandler.h"
#include "Collision.h"
#include "Samus.h"
#include "Tsumuri.h"
#include "Tilemap.h"
#include "sprites/horonad/horonad-standing.h"
#include "sprites/metroids/alpha.h"
#include "HUD.h"

extern "C" {
#include "tonc.h"
#include "tonc_video.h"
#include "tonc_input.h"
#include "tonc_bios.h"
#include "tonc_nocash.h"
}

#include <string.h>
#include <stdlib.h>



//void VBlankIntrWait()
//{   swi_call(0x05); }



//#define CBB_0  0
//#define SBB_0 28

//SCR_ENTRY *bg0_map= se_mem[SBB_0];






//class Player;



bool collisionDetectionWithDoor( int x0, int y0, int x1, int y1, int door[2][2]){
	if( x0 < door[0][0]+door[1][0] &&
		x1 > door[0][0] &&
		y0 < door[0][1]+door[1][1] &&
	    y1 > door[0][1]) {
	    // collision detected!
		return true;
	}
	return false;
}

void writeSaveState( Player *player, int roomID){
	sprintf((char*)MEM_SRAM, "%d %d %d\n", roomID, player->x(), player->y());
}


class Tilemap;

class Savepoint: public Object{
	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	int _x, _y;
	int _roomID;
	bool _alreadyUsed;
public:
	static void init( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( savepointTiles, savepointTilesLen);
		sPb  = spriteEngine->addPalette( savepointPal,   savepointPalLen);
	}
	Savepoint( int x, int y, int roomID): _x(x), _y(y), _roomID(roomID) {
		_alreadyUsed = false;
		setObjectCount(1);
		obj_set_attr( obj(0),
					ATTR0_TALL | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x64,              // 64x64p,
					ATTR2_PALBANK(sPb) | sTid | ATTR2_PRIO(2));
		setCollidable( new CollisionBox( this, 0,26, 32, 64-26));
		setEventBox( new CollisionBox( this, 0,0, 32, 26));
	}
	void update(Tilemap *room){

	}
	void updateScreen(Tilemap *room){
		 obj_set_pos(obj(0), int(_x-room->x), int(_y-room->y));
	}
	int x(){return _x;}
	int y(){return _y;}

	void triggerEvent( Object *triggeringObject){
		if(!_alreadyUsed){
//			int targetTilemap = 0;
			writeSaveState( (Player*)triggeringObject, _roomID);
			_alreadyUsed = true;
		}
	}
};
signed int Savepoint::sTid;
signed int Savepoint::sPb;



class Gunship: public Object{
	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	float _dy;
	float _range;
	float _direction;
	float _x, _y;
//	int objID1, objID2;
	int polygon[7][2] = {
			{11,34}, {64,0}, {80,4}, {112, 4}, {128, 0}, {180,34}, {11,34}
	};
public:
	static void init( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( gunshipTiles, gunshipTilesLen);
		sPb  = spriteEngine->addPalette( gunshipPal,   gunshipPalLen);
	}

	Gunship( int x, int y): _x(x), _y(y) {

		_dy = 0.;
		_range = 5.;
		_direction = +1.;

//		objID1 = ObjectHandler::instance().add();
//		objID2 = ObjectHandler::instance().add();

		setObjectCount(3);
		obj_set_attr( obj(0),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					ATTR2_PALBANK(sPb) | sTid);
		obj_set_attr( obj(1),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					ATTR2_PALBANK(sPb) | (sTid+8*8));
		obj_set_attr( obj(2),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					ATTR2_PALBANK(sPb) | (sTid+16*8));

//		Collidables::instance().add( this);
//		setCollidable( new CollisionBox(this, 0, 0, 192,64));
		setCollidable( new CollisionPolygon(this, polygon, 7));
	}

	~Gunship(){
//		Collidables::instance().remove( this);
	};

	int x(){
		return _x;
	}
	int y(){
		return _y;
	}

	 void update( Tilemap *tilemap){

		 _dy += _direction*0.1;
		 if( _dy/_direction >= _range)
			 // change direction
			 _direction *= -1;

	 };
	 void updateScreen(Tilemap *room){
//		 ObjectHandler::instance().get( objID)->attr1 = ATTR1_SIZE_64x64 | (_direction == -1 ? ATTR1_HFLIP : 0);
		 if(!onScreen(room->x, room->y)){
			 obj_hide(obj(0));
		 	 obj_hide(obj(1));
		 	 obj_hide(obj(2));
		 }else{
			 obj_unhide(obj(0), ATTR0_BLEND);
			 obj_unhide(obj(1), ATTR0_BLEND);
			 obj_unhide(obj(2), ATTR0_BLEND);
			 obj_set_pos(obj(0), int(_x-room->x), int(_y+_dy-room->y));
			 obj_set_pos(obj(1), int(_x+64-room->x), int(_y+_dy-room->y));
			 obj_set_pos(obj(2), int(_x+128-room->x), int(_y+_dy-room->y));
		 }
	 };




		bool onScreen( int x,int y){
			return _x < x+240 and _x+192 > x and _y < y+160 and _y+64 > y;
		}

//		int objCount(){
//			return 3;
//		}
//		OBJ_ATTR obj( int index){
//			switch(index){
//			case 0:
//				return *ObjectHandler::instance().get( objID);
//			case 1:
//				return *ObjectHandler::instance().get( objID1);
//			case 2:
//				return *ObjectHandler::instance().get( objID2);
//			}
//		}

};

signed int Gunship::sTid;
signed int Gunship::sPb;

class Pupu: public Object{
	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	int _dx;
	int _range;
	int _direction;
	int _x, _y;
public:
	static void init( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( pupuTiles, pupuTilesLen);
		sPb  = spriteEngine->addPalette( pupuPal,   pupuPalLen);
	}

	Pupu( int x, int y): _x(x), _y(y) {

		_dx = 0;
		_range = 100;
		_direction = +1;

		setObjectCount(1);
		obj_set_attr( obj(),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x32,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(sPb) | sTid);
	}

	int x(){
		return _x;
	}
	int y(){
		return _y;
	}
	 void update( Tilemap *tilemap){

		 _dx += _direction;
		 if( _dx == _direction*_range)
			 // change direction
			 _direction *= -1;

	 };
	 void updateScreen(Tilemap *room){
		 obj()->attr1 = ATTR1_SIZE_32x32 | (_direction == -1 ? ATTR1_HFLIP : 0);

		 obj_set_pos( obj(), _x+_dx-room->x, _y-room->y);
	 };
	 bool collision( int x0, int y0, int x1, int y1){ return true;};

//	 int objCount(){ return 1;}
//	 OBJ_ATTR obj( int index){
//		 return *ObjectHandler::instance().get( objID);
//	 }
};

signed int Pupu::sTid;
signed int Pupu::sPb;


float sqrt1(const float x)
{
  union
  {
    int i;
    float x;
  } u;
  u.x = x;
  u.i = (1<<29) + (u.i >> 1) - (1<<22);

  // Two Babylonian Steps (simplified from:)
  // u.x = 0.5f * (u.x + x/u.x);
  // u.x = 0.5f * (u.x + x/u.x);
  u.x =       u.x + x/u.x;
  u.x = 0.25f*u.x + x/u.x;

  return u.x;
}

class AlphaMetroid: public Object{
	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	int _dx;
	int _range;
	int _direction;
	float _x, _y;
	float _curSpeed[2];
	const float _maxSpeed[2] = {2,2};
	Player *_player;
public:
	static void init( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( alphaTiles, alphaTilesLen);
		sPb  = spriteEngine->addPalette( alphaPal,   alphaPalLen);
	}

	AlphaMetroid( int x, int y, Player *samus): _x(x), _y(y), _player(samus) {

		_dx = 0;
		_range = 100;
		_direction = +1;
		_curSpeed[0] = 0;
		_curSpeed[1] = 0;

		setObjectCount(1);
		obj_set_attr( obj(),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(sPb) | sTid);

		setHitBox( new CollisionBox(this, 0,0, 64, 64));
	}

	int x(){
		return (int)_x;
	}
	int y(){
		return (int)_y;
	}
	 void update( Tilemap *tilemap){

//		 _dx += _direction;
//		 if( _dx == _direction*_range)
//			 // change direction
//			 _direction *= -1;

		 int dx = x() - _player->x();
		 int dy = y() - _player->y();

		 float acc[2] = {
				 -dx,
				 -dy
		 };

		 for( int i=0; i<2; i++){
			 _curSpeed[i] += acc[i]*0.005;
			 _curSpeed[i] = MINMAX( -_maxSpeed[i], _curSpeed[i], _maxSpeed[i]);
		 }

		 _x += _curSpeed[0];
		 _y += _curSpeed[1];

		 //		 int d = sqrt1(dx*dx + dy*dy);
//
//		 dx *= 4/d;
//		 dy *= 4/d;

//		 _x += dx;
//		 _y += dy;

//		 if(dx < 0)
//			 _x += +1;
//		 if(dx > 0)
//			 _x += -1;
//		 if(dy < 0)
//			 _y += +1;
//		 if(dy > 0)
//			 _y += -1;

	 };
	 void updateScreen(Tilemap *room){
		 obj()->attr1 = ATTR1_SIZE_64x64 | (_direction == -1 ? ATTR1_HFLIP : 0);

		 obj_set_pos( obj(), _x+_dx-room->x, _y-room->y);
	 };
	 bool collision( int x0, int y0, int x1, int y1){ return true;};

//	 int objCount(){ return 1;}
//	 OBJ_ATTR obj( int index){
//		 return *ObjectHandler::instance().get( objID);
//	 }
};

signed int AlphaMetroid::sTid;
signed int AlphaMetroid::sPb;

class Beam: public Object{
	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
public:
	static void init( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( beamTiles, beamTilesLen);
		sPb  = spriteEngine->addPalette( beamPal,   beamPalLen);
	}


	//enum Direction {Left, Right, Up, Down, LeftUp, LeftDown, RightUp, RightDown};
	signed int tid;
	signed int pb;

	int position[2];
	int size[2];
	int bb[2][2];
	int direction[2];

	int frameCount;
public:
	Beam( signed int tid, signed int pb, int *position, int *direction): tid(tid), pb(pb) {

		tid = sTid;
		pb  = sPb;

		frameCount=0;
		for(int i=0; i<2; i++){
			this->direction[i] = direction[i];
			this->position[i] = position[i];

		}

		setObjectCount(1);
		obj_set_attr(obj(),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_16x16,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(pb) | tid);

		u16 attr1 = ATTR1_SIZE_16x16;
		u16 attr2 = ATTR2_PALBANK(pb);

		if( direction[0] == 0){
			// vertical
			attr2 |= (tid + 4*2*2);
			bb[0][0] = 0; bb[0][1] = 4; bb[1][0] = 12; bb[1][1] = 8; //{{0,4},{12,8}};
		}else if( direction[1] == 0){
			// horizontal
			attr2 |= tid;
			bb[0][0] = 8; bb[0][1] = 4; bb[1][0] = 16; bb[1][1] = 12; //{{0,4},{12,8}};
		}else{
			// diagonal
			attr2 |= (tid + 2*2*2);
			bb[0][0] = 0; bb[0][1] = 4; bb[1][0] = 12; bb[1][1] = 8; //{{0,4},{12,8}};
		}


		if( direction[0] == -1){
			attr1 |= ATTR1_HFLIP;
			int t0 = 16-bb[0][0];
			int t1 = 16-bb[1][0];
			bb[0][0] = t1;
			bb[1][0] = t0;
		}
		if( direction[1] == +1){
			attr1 |= ATTR1_VFLIP;
			int t0 = 16-bb[0][1];
			int t1 = 16-bb[1][1];
			bb[0][1] = t1;
			bb[1][1] = t0;
		}

		obj()->attr1 =attr1;
		obj()->attr2 =attr2;

		size[0] = 16;
		size[1] = 16;
	}

	int x(){
		return position[0];
	}
	int y(){
		return position[1];
	}

	void update( Tilemap *tilemap){
		for(int i=0; i<2; i++)
			if( !tilemap->collisionDetection( position[0]+bb[0][0], position[1]+bb[0][1], position[0]+bb[1][0], position[1]+bb[1][1]) and ! CollisionBox::CollisionBoxes->collision( position[0]+bb[0][0], position[1]+bb[0][1], position[0]+bb[1][0], position[1]+bb[1][1]))
				// move
				position[i] += direction[i]*4;
			else{
				// check for blocks
				for( int i=0; i<CollisionBox::CollisionBoxes->size(); i++){
					if( CollisionBox::CollisionBoxes->get(i)->collision( position[0]+bb[0][0], position[1]+bb[0][1], position[0]+bb[1][0], position[1]+bb[1][1])){
						CollisionBox::CollisionBoxes->get(i)->applyDamage( position[0]+bb[0][0], position[1]+bb[0][1], position[0]+bb[1][0], position[1]+bb[1][1]);
					}
				}

				// destroy
				suspend();
			}
		//position[0]++;
//		obj_set_pos(objectHandler->get( objID), position[0], position[1]);
		frameCount++;
		if(frameCount > 120)
			suspend();
	}

	void updateScreen( Tilemap *room){
		if(frameCount>100)
			obj_hide( obj());
		else{
			obj_set_pos( obj(), position[0]-room->x, position[1]-room->y);
		}
	}

	bool collision( int x0, int y0, int x1, int y1){
		if (x0 < position[0]+size[0] &&
		   x1 > position[0] &&
		   y0 < position[1]+size[1] &&
		   y1 > position[1]) {
			// collision detected!
			return true;
		}
		return false;
	}

	void onCollision( Object *obj){
	//	obj_hide( ObjectHandler::instance().get( objID));
	}
};
 signed int Beam::sTid;
 signed int Beam::sPb;



enum Direction {RIGHT, LEFT, UP, DOWN};
typedef struct {
	int area[2][2];
	int targetTilemap;
	int targetArea;
	int direction;
} Transition;

//Transition surface1Tra[] = {
//		{ {{63*16,19*16},{64*16,22*16}}, 1, 0, RIGHT}
//};
//Transition surface2Tra[] = {
//		{ {{ 0*16, 3*16},{ 1*16, 6*16}}, 0, 0, LEFT},
//		//{ {{53*16,15*16},{56*16,16*16}}, 2, 0, DOWN}
//		{ {{79*16,19*16},{80*16,24*16}}, 3, 0, RIGHT}
//};
//Transition surface3Tra[] = {
//		{ {{5*16,0*16},{8*16,1*16}}, 1, 1, UP},
//		{ {{31*16,3*16},{32*16,8*16}}, 3, 0, RIGHT}
//};
//Transition surface4Tra[] = {
//		//{ {{0*16,3*16},{1*16,8*16}}, 2, 1, LEFT},
//		{ {{0*16,3*16},{1*16,8*16}}, 1, 1, LEFT},
//		{ {{6*16,64*16},{10*16,65*16}}, 4, 0, DOWN}
//};
//Transition surface5Tra[] = {
//		{ {{22*16,-1*16},{26*16,0*16}}, 3, 1, UP},
//		{ {{ -1*16, 20*16},{ 0*16, 25*16}}, 5, 0, LEFT}
//};
//Transition surface6Tra[] = {
//		{ {{48*16,20*16},{49*16,25*16}}, 4, 1, RIGHT}
//};

#include "tilemaps/world.h"





class World{
//	const unsigned short *maps[6] = {surface1Map, surface2Map, surface3Map, surface4Map, surface5Map, surface6Map};
//	const unsigned short *palettes[6] = {surface1Pal, surface2Pal, surface3Pal, surface4Pal, surface5Pal, surface6Pal};
//	const unsigned int   *tiles[6] = {surface1Tiles, surface2Tiles, surface3Tiles, surface4Tiles, surface5Tiles, surface6Tiles};
//	int tilesLens[6] = {surface1TilesLen, surface2TilesLen, surface3TilesLen, surface4TilesLen, surface5TilesLen, surface6TilesLen};

	unsigned short *collisionMap;

	const unsigned int BYTES_PER_MAPBLOCK = 32*32*2;
public:
	//Transition *transitions[6] = {surface1Tra, surface2Tra, surface3Tra, surface4Tra, surface5Tra, surface6Tra};
	//int transitionLen[6] = {1,2,2,2,2,1};
	Tilemap *room;
	int roomIndex;
	World(){
		collisionMap = 0;
		room = 0;
		//maps = {surface1Map, surface2Map};
		//roomIndex = 0;

	}

	void loadTilemap( int index){
		roomIndex = index;

		// Load palette
		//memcpy(pal_bg_bank[0], palettes[roomIndex], 32);
		// Load tiles into CBB 0
		//memcpy(tile_mem[0], tiles[roomIndex], tilesLens[roomIndex]);

		// free memory
		if( collisionMap != 0)
			free( collisionMap);
		// allocate memory
		collisionMap = (unsigned short *) malloc( mapBlocks[roomIndex][0]*mapBlocks[roomIndex][1]*BYTES_PER_MAPBLOCK);
		// extract map
		LZ77UnCompWram( colMap[roomIndex], collisionMap);


		if( room != 0)
			delete room;
		room = new Tilemap(collisionMap, mapBlocks[roomIndex][0], mapBlocks[roomIndex][1], 4, 0);
//		room = new Tilemap(colMap[roomIndex], mapBlocks[roomIndex][0], mapBlocks[roomIndex][1], 4, 0);

	}
};

class Horntoad: public Object{

	static signed int sTid;
	static signed int sPb;

public:
	static void init( SpriteEngine *spriteEngine){
			sTid = spriteEngine->addTiles( horonad_standingTiles, horonad_standingTilesLen);
			sPb  = spriteEngine->addPalette( horonad_standingPal,   horonad_standingPalLen);
			//spriteEngine->addTiles( horonad_standingTiles, horonad_standingTilesLen);
		}


	//ObjectHandler *objHandler;
	int objID;
	int x=0,y=0;

	int tid = 14*8*8;
	int pb = 1;

	int animationIndex;
public:
	Horntoad(){

		objID = ObjectHandler::instance().add();
		//sprite = &obj_buffer[objID];
		obj_set_attr( ObjectHandler::instance().get( objID),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x32,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(pb) | tid);
					//obj_set_pos(sprite, x*16, y*16);
		obj_set_pos( ObjectHandler::instance().get( objID), x, y);

		animationIndex=0;
	}

	void update( Tilemap* room){
		animationIndex++;

		tid = Horntoad::sTid + (animationIndex/4 % 6)*4*4;
		pb = Horntoad::sPb;
		ObjectHandler::instance().get( objID)->attr2 = ATTR2_PALBANK(pb) | tid;
	}
	void updateScreen( Tilemap* room){

	}

	bool collision( int x0, int y0, int x1, int y1) {
		return false;
	}

	int objCount() {return 1;};
	OBJ_ATTR obj( int index) {
		return *ObjectHandler::instance().get( objID);
	}
};

signed int Horntoad::sTid = 0;
signed int Horntoad::sPb = 0;


Object *objectList[128];
int objectCount = 0;

void resetObjectList(){
	objectCount = 0;
}
void addObject( Object *obj){
	objectList[ objectCount] = obj;
	objectCount++;
}
void removeObject( int index){
	objectCount--;
	objectList[ index] = objectList[ objectCount];
}

/*
typedef struct {
	char index;
	OBJ_ATTR* pObjectBufferEntry;
} Object;*/


#include "BackgroundHandler.h"
BackgroundHandler * backgroundHandler = new BackgroundHandler();

typedef struct {
	const unsigned short *map;
	const unsigned short *pal;
	int palLen;
	const unsigned int *tiles;
	int tilesLen;
} BackgroundDefinition;

void loadRoom( int targetTilemap, World *&world, Tilemap *&forground1, Tilemap* &forground2, Tilemap* &background2, Player*&player, Object *objectList[128], int &objectCount){

	vid_vsync();
	REG_DISPCNT= DCNT_MODE0 | DCNT_OBJ | DCNT_OBJ_1D;


	backgroundHandler->reset();
	world->loadTilemap( targetTilemap);

	for( int i=0; i<objectCount; i++){
		if( objectList[i] != player)
			delete objectList[i];
	}
	resetObjectList();
	addObject( player);

	Palette *p=0, *p0=0, *p1=0;
	Tileset *t=0, *t0=0, *t1=0;

	p0 = backgroundHandler->addPalette( bg0Pal[targetTilemap],   bg0PalLen[targetTilemap], 0);
	p1 = backgroundHandler->addPalette( bg1Pal[targetTilemap],   bg1PalLen[targetTilemap], 1);

	t0 = backgroundHandler->addTileset( bg0Tiles[targetTilemap],   bg0TilesLen[targetTilemap]);
	t1 = backgroundHandler->addTileset( bg1Tiles[targetTilemap],   bg1TilesLen[targetTilemap]);

	forground1 = backgroundHandler->addBackground( bg0Map[targetTilemap], mapBlocks[targetTilemap][0],mapBlocks[targetTilemap][1], t0);
	forground2 = backgroundHandler->addBackground( bg1Map[targetTilemap], mapBlocks[targetTilemap][0],mapBlocks[targetTilemap][1], t1, 1);

	BackgroundDefinition bgdef;



	if(targetTilemap==0){

//		p = backgroundHandler->addPalette( background1Pal,   background1PalLen, 2);
//		t = backgroundHandler->addTileset( background1Tiles, background1TilesLen);
//		background2 = backgroundHandler->addBackground( background1Map, 2,2, t, 2);
		bgdef = { background1Map, background1Pal, background1PalLen, background1Tiles, background1TilesLen};

//		addObject( new Pupu( 100, 100));
		addObject( new Gunship( 14*16,21*16));
	}
	if(targetTilemap==1){
//		p = backgroundHandler->addPalette( background1Pal,   background1PalLen, 2);
//		t = backgroundHandler->addTileset( background1Tiles, background1TilesLen);
//		background2 = backgroundHandler->addBackground( background1Map, 2,2, t, 2);
		bgdef = { background1Map, background1Pal, background1PalLen, background1Tiles, background1TilesLen};

		addObject( new Pupu( 100, 100));

		addObject( new CrackedBlock(player, 51*16,12*16, 5,1));
		addObject( new CrackedBlock(player, 53*16,17*16, 1,1));
		addObject( new CrackedBlock(player, 55*16,21*16, 4,1));
		addObject( new CrackedBlock(player, 51*16,24*16, 4,1));
		addObject( new CrackedBlock(player, 76*16,28*16, 3,1));
		addObject( new CrackedBlock(player, 72*16,25*16, 7,1));

	}

	if(targetTilemap==2){
//		p = backgroundHandler->addPalette( background1Pal,   background1PalLen, 2);
//		t = backgroundHandler->addTileset( background1Tiles, background1TilesLen);
//		background2 = backgroundHandler->addBackground( background1Map, 2,2, t, 2);
		bgdef = { background1Map, background1Pal, background1PalLen, background1Tiles, background1TilesLen};

		addObject( new Tsumuri(4*16, 10*16));

	}if(targetTilemap==3){
//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};


	}if(targetTilemap==4){
//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

		addObject( new Savepoint( 36*16,21*16, 4));
	}if(targetTilemap==5){
//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

	}if(targetTilemap==6){

//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

	}if(targetTilemap==7){

//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

		//addObject(new AlphaMetroid( 0,0, player));
		addObject( new Savepoint( 7*16,6*16, 7));
	}if(targetTilemap==8){

//		p = backgroundHandler->addPalette( background2Pal,   background2PalLen, 2);
//		t = backgroundHandler->addTileset( background2Tiles, background2TilesLen);
//		background2 = backgroundHandler->addBackground( background2Map, 2,2, t, 2);
		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

		forground2->setWrapX( true);
		forground2->setMoveX( 32);
//		addObject( new Savepoint( 7*16,4*16, 7));
	}if(targetTilemap==9){

		bgdef = { background2Map, background2Pal, background2PalLen, background2Tiles, background2TilesLen};

	}

//	p0 = backgroundHandler->addPalette( bg0Pal[targetTilemap],   bg0PalLen[targetTilemap], 0);
//	p1 = backgroundHandler->addPalette( bg1Pal[targetTilemap],   bg1PalLen[targetTilemap], 1);
	p  = backgroundHandler->addPalette( bgdef.pal,   bgdef.palLen, 2);

//	t0 = backgroundHandler->addTileset( bg0Tiles[targetTilemap],   bg0TilesLen[targetTilemap]);
//	t1 = backgroundHandler->addTileset( bg1Tiles[targetTilemap],   bg1TilesLen[targetTilemap]);
	t  = backgroundHandler->addTileset( bgdef.tiles,   bgdef.tilesLen);

//	forground1 = backgroundHandler->addBackground( bg0Map[targetTilemap], mapBlocks[targetTilemap][0],mapBlocks[targetTilemap][1], t0);
//	forground2 = backgroundHandler->addBackground( bg1Map[targetTilemap], mapBlocks[targetTilemap][0],mapBlocks[targetTilemap][1], t1, 1);
	background2 = backgroundHandler->addBackground( bgdef.map, 2,2, t, 2);

	background2->setWrapX( true);
	background2->setWrapY( true);


	delete p0;
	delete p1;
	delete t;
	delete t0;
	delete t1;



	vid_vsync();
	REG_DISPCNT= DCNT_MODE0 | DCNT_OBJ | DCNT_OBJ_1D | backgroundHandler->getControlFlags();
//	REG_DISPCNT= DCNT_MODE0 | DCNT_OBJ | DCNT_OBJ_1D | DCNT_BG0 | DCNT_BG1 | DCNT_BG2;// | DCNT_BG3;
	//REG_BG0CNT = BG_CBB(0) | BG_SBB(4) | BG_4BPP | BG_REG_64x64 | BG_PRIO(0);
	//REG_BG1CNT = BG_CBB(1) | BG_SBB(17) | BG_4BPP | BG_REG_64x64 | BG_PRIO(1);
}



int main(){

//	fprintf( stderr, "HELLO WORLD\n");
//	fprintf( stdout, "HELLO WORLD\n");
//	sprintf( nocash_buffer, "HELLO WORLD\n");
////	fputs ("HELLO WORLD\n" , nocash_buffer);
//	nocash_puts("Hello World!\n");
//	nocash_message();

	// BACKGROUNDS
	Tilemap *forground1;// = new Tilemap(surface1_0Map, 4,2, 0);
	Tilemap *forground2;// = new Tilemap(surface1_1Map, 4,2, 1);
	Tilemap *background2 = 0;


    // SPRITES
	SpriteEngine *spriteEngine = new SpriteEngine();

	Horntoad::init( spriteEngine);
	Tsumuri::init( spriteEngine);
	Player::init( spriteEngine);
	Pupu::init( spriteEngine);
	Gunship::init( spriteEngine);
	//Player::init( spriteEngine);
	Beam::init( spriteEngine);
	BlockBase::loadSprites( spriteEngine);
	Savepoint::init( spriteEngine);
	AlphaMetroid::init(spriteEngine);
	HUD::init(spriteEngine);





	// (2) Initialize all sprites


	// LOAD SAVE STATE
	char* pEnd = (char*)MEM_SRAM;
	int roomID = strtol (pEnd, &pEnd, 10);
	float playerPositionX = strtof (pEnd, &pEnd);
	float playerPositionY = strtof (pEnd, &pEnd);


//	roomID = 0;
//	playerPositionX = 50;
//	playerPositionY = 0;

	Player *player = new Player();
	player->position[0] = playerPositionX;
	player->position[1] = playerPositionY;

	HUD *hud = new HUD( player);



	World *world = new World();
	loadRoom( roomID, world, forground1, forground2, background2, player, objectList, objectCount);
	//addObject( hud);

	SoundEngine *snd = new SoundEngine();


	unsigned int cycleCount = 0;
	int lastObjCount = 0;

	while(1)
	{
		vid_vsync();
		key_poll();

		// SHOOTING
		if( key_hit(KEY_B)){
			// shot
			//int objectID = objectHandler->add();
			int p[2];
			if( player->direction == RIGHT){
				p[0] = player->x()+44-8;
				p[1] = player->y()+35-8;
			}else{
				p[0] = player->x()+20-8;
				p[1] = player->y()+35-8;
			}
			int d[] = {key_tri_horz(),key_tri_vert()};
			if( key_tri_horz() == 0 and key_tri_vert() == 0){
				d[0] = player->direction;
			}

			addObject( new Beam( 14*8*8 + 1*4*4, 2, p, d));
		}


		// update objects
		for( int i=0; i<objectCount; i++)
			objectList[i]->update(world->room);

		// remove suspended objects
		for( int i=0; i<objectCount; i++)
			if( objectList[i]->suspended()){
				delete objectList[i];
				removeObject( i);
			}

		// events
		for( int i=0; i<Collidable::EventBoxes->size(); i++){
			if( Collidable::EventBoxes->get(i)->collision(player->x()+player->bb[0][0], player->y()+player->bb[0][1], player->x()+player->bb[1][0], player->y()+player->bb[1][1])){
				// trigger event
				Collidable::EventBoxes->get(i)->object()->triggerEvent( player);
			}
		}

		// hits
		if( player->invulnerableCycles == 0){
			for( int i=0; i<Collidable::HitBoxes->size(); i++){
				if( Collidable::HitBoxes->get(i)->collision(
						player->x()+player->bb[0][0],
						player->y()+player->bb[0][1],
						player->x()+player->bb[1][0],
						player->y()+player->bb[1][1])){
					// damage Samus
					//Collidable::EventBoxes->get(i)->object()->triggerEvent( player);
					if( player->health > 0)
						player->health --;
//					player->health = MAX( player->health-1, 0);
					player->invulnerableCycles = 60*3;
				}
	//			else{
	//				player->health ++;
	//			}
			}
		}

		// update layers
		forground1->update( player->x()+32,player->y()+32);
		forground2->update( player->x()+32,player->y()+32);
		background2->update( forground1->x/2, forground1->y/2);


		for( int i=0; i<objectCount; i++)
			objectList[i]->updateScreen(forground1);
		hud->updateScreen(forground1);

		//oam_copy(oam_mem, obj_buffer, 2);
		int count = 0;
		for( int i=0; i<objectCount; i++){
			for( int j=0; j<objectList[i]->objCount(); j++){
//				OBJ_ATTR obj = objectList[i]->obj(j);
				oam_copy( &oam_mem[count], objectList[i]->obj(j), 1);
				count++;
			}
		}
		OBJ_ATTR empty;
		empty.attr0 = ATTR0_HIDE;
		for( int i=count; i<lastObjCount; i++){
			oam_copy( &oam_mem[i], &empty, 1);
		}
		lastObjCount = count;

		if( !world->room->inMap( player->x()+(player->bb[0][0]+player->bb[1][0])/2, player->y()+(player->bb[0][1]+player->bb[1][1])/2)){


			for( int i=0; i<transitionLen[world->roomIndex]; i++){
				Transition *triggerTransition = &transitions[world->roomIndex][i];
				if( collisionDetectionWithDoor( player->x()+player->bb[0][0], player->y()+player->bb[0][1], player->x()+player->bb[1][0], player->y()+player->bb[1][1], triggerTransition->area)){
					Transition *triggerTransition = &transitions[world->roomIndex][i];
					int targetTilemap = triggerTransition->targetTilemap;

					player->position[0] += roomOffsets[world->roomIndex][0] - roomOffsets[targetTilemap][0];
					player->position[1] += roomOffsets[world->roomIndex][1] - roomOffsets[targetTilemap][1];

					// unload current room
					delete forground1;
					delete forground2;
					delete background2;
					//delete enemie;

					loadRoom( targetTilemap, world, forground1, forground2, background2, player, objectList, objectCount);

					//world->room->update( player->x()+32,player->y()+32);

					//writeSaveState( player, targetTilemap);
	//				roomID = targetTilemap;
					break;
				}
			}
		}



		if( !snd->running)
			//snd->startTrack( snd_01_AM2R_Title, snd_01_AM2R_Title_bytes);
			snd->startTrack( snd_04_AM2R_Initial_Decent, snd_04_AM2R_Initial_Decent_bytes);
		snd->update();

		cycleCount ++;


		//nocash_puts("Hello world!\n");
		//nocash_message();
	}
}
