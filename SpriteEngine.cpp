/*
 * SpriteEngine.cpp
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#include <string.h>

#include "SpriteEngine.h"
#include "tonc_core.h"

SpriteEngine::SpriteEngine(){
	palBankCount = 0;
	tilesCount = 0;
}

unsigned int SpriteEngine::addTiles( const unsigned int *tiles, int tilesLen){

	unsigned int tid = tilesCount;

	memcpy( &tile_mem[4][tilesCount], tiles, tilesLen);
	tilesCount += tilesLen / BYTES_PER_TILE4;

	return tid;
}

unsigned int SpriteEngine::allocateTiles( int tilesCount){

	unsigned int tid = this->tilesCount;

	this->tilesCount += tilesCount;

	return tid;
}


void SpriteEngine::copyTiles( unsigned int tidSrc, unsigned int tidDst, const unsigned int *tiles, int tilesCount){
	memcpy( &tile_mem[4][tidDst], &tiles[tidSrc*INTS_PER_TILE4], tilesCount*BYTES_PER_TILE4);
}

unsigned int SpriteEngine::addPalette( const unsigned short *pal, int palLen){

	unsigned int pb = palBankCount;
	memcpy( &pal_obj_bank[palBankCount], pal, palLen);
	palBankCount ++;

	return pb;
}

void SpriteEngine::copyPalette( unsigned int pbDst, const unsigned short *palette, int paletteLen){
	memcpy( &pal_obj_bank[ pbDst], palette, paletteLen);
}
