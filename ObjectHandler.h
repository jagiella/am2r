/*
 * ObjectHandler.h
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#ifndef OBJECTHANDLER_H_
#define OBJECTHANDLER_H_


#include "Tilemap.h"


class ObjectHandler{
	static ObjectHandler *pInstance;

	OBJ_ATTR obj_buffer[128];
	int countObjects;
	//Object   objects[128];
	int   objectIndices[128];
	int *pObjectIndices[128];


	ObjectHandler(){
		countObjects = 0;
		oam_init(obj_buffer, 128);
		for(int i=0; i<128; i++)
			//pObjects[i] = &objects[i];
			pObjectIndices[i] = &objectIndices[i];
	}


public:
	static ObjectHandler &instance(){
		if (pInstance == nullptr)
			pInstance = new ObjectHandler();
		return *pInstance;
	}


	int add(){
		//*pObjectIndices[ countObjects] = countObjects;
		//countObjects++;
		//return pObjectIndices[ countObjects];

		//pObjects[ countObjects].index = countObjects;
		//pObjects[ countObjects].pObjectBufferEntry = obj_buffer[ countObjects];
		//return &objects[ countObjects];

		countObjects++;
		return countObjects-1;
	}

	OBJ_ATTR* get(int index){
		return &obj_buffer[index];
	}

	void remove( int pObjectIndex){
		countObjects--;
	}

	void update(){
		oam_copy( oam_mem, obj_buffer, countObjects);
	}
};

class Collidable;
class Collidables;
class CollisionBox;

class Object{
private:
	// sprites
	OBJ_ATTR *_objects; // sprite objects
	int _objCount;

	// position
//	int _x, _y;

	// collision, hit, hurt boxes
	Collidable *_collidable;
	Collidable *_eventBox; // triggers events
	Collidable *_hitBox; // damages Samus

	// state
	bool _suspended;

protected:
	void setObjectCount( int count);
	void setCollidable( Collidable* collidable);
	void setEventBox( Collidable* collidable);
	void setHitBox( Collidable* collidable);

	void suspend() {_suspended=true;};
public:
	Object();
	int objCount();
	OBJ_ATTR *obj( int index=0);
	Collidable *collidable() { return _collidable;};
	bool suspended(){ return _suspended;};

	virtual ~Object();
	virtual int x() = 0;
	virtual int y() = 0;
	virtual void update( Tilemap *tilemap)=0;
	virtual void updateScreen(Tilemap *room)=0;
//	virtual bool collision( int x0, int y0, int x1, int y1) = 0;
	virtual void applyDamage( int x0, int y0, int x1, int y1){};
	virtual void triggerEvent( Object *triggeringObject){};
		//virtual void onCollision(Object *obj) = 0;

};


//class Collidable;

class Collidable{
protected:
	Object *_object; // referenced Object
	Collidables *_list; // list to add to
public:
	static Collidables *CollisionBoxes; // Weapon<-Obstacle, enemie<-obstacle, Samus<-obstacle
	static Collidables *EventBoxes; // Samus<-anything
	static Collidables *HurtBoxes; // object receives damage: Samus<-anything, enemies<-weapons
	static Collidables *HitBoxes;  // deals damage: blocks/enemies<-weapon, samus<-enemies

	Collidable( Object *object, Collidables *list = 0);
	virtual ~Collidable();
	Object *object();
	void setList( Collidables *list);
	virtual bool collision( int x0, int y0, int x1, int y1);
	virtual void applyDamage( int x0, int y0, int x1, int y1);
};

class CollisionBox: public Collidable{
protected:

public:
	int x,y,w,h;
	CollisionBox( Object *object, int x, int y, int w, int h);
	~CollisionBox();

	bool collision( int x0, int y0, int x1, int y1);
};

class CollisionTilemap: public Collidable{
protected:

public:
	int x,y,w,h,nx,ny;
	bool **tilemap;
	CollisionTilemap( Object *object, int x, int y, int w, int h, int nx, int ny);
	~CollisionTilemap();

	bool collision( int x0, int y0, int x1, int y1);

	void set( int i, int y, bool value);
	bool get( int i, int j);
};

class CollisionPolygon: public Collidable{
protected:

public:
	int length;
	int **polygon;
	CollisionPolygon( Object *object, int polygon[][2], int length);
	~CollisionPolygon();

	bool collision( int x0, int y0, int x1, int y1);
};



class Collidables{
//	static Collidables *pCollisionBoxes;
//	static Collidables *pHitBoxes;
//	static Collidables *pHurtBoxes;
	int count;
	Collidable *pObjects[128];

public:
	Collidables(){
		count = 0;
	}
//	static Collidables &CollisionBoxes(){
//		if( pCollisionBoxes == nullptr)
//			pCollisionBoxes = new Collidables();
//		return *pCollisionBoxes;
//	}
//	static Collidables &HitBoxes(){
//		if( pHitBoxes == nullptr)
//			pHitBoxes = new Collidables();
//		return *pHitBoxes;
//	}

	void add( Collidable *obj){
		this->pObjects[ this->count] = obj;
		this->count++;
	}
	void remove( Collidable *obj){
		for( int i=0; i<this->size(); i++){
			if( this->pObjects[ i ] == obj){
				this->count --;
				this->pObjects[ i ] = this->pObjects[ this->count ]; // swap with last element
				return;
			}
		}
	}
	int size(){
		return this->count;
	}
	Collidable *get( int index){
		return this->pObjects[ index];
	}

	bool collision( int x0, int y0, int x1, int y1){
		for( int i=0; i<this->size(); i++){
			if( this->get(i)->collision( x0, y0, x1, y1))
				return true;
		}
		return false;
	}
};





#endif /* OBJECTHANDLER_H_ */
