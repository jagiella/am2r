
//{{BLOCK(tsumuri)

//======================================================================
//
//	tsumuri, 32x32@4, 
//	+ palette 16 entries, not compressed
//	+ 16 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2019-10-21, 22:48:10
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_TSUMURI_H
#define GRIT_TSUMURI_H

#define tsumuriTilesLen 512
extern const unsigned int tsumuriTiles[128];

#define tsumuriPalLen 32
extern const unsigned short tsumuriPal[16];

#endif // GRIT_TSUMURI_H

//}}BLOCK(tsumuri)
