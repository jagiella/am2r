
//{{BLOCK(jumping)

//======================================================================
//
//	jumping, 320x64@4, 
//	+ palette 16 entries, not compressed
//	+ 320 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 10240 = 10272
//
//	Time-stamp: 2019-11-24, 22:57:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_JUMPING_H
#define GRIT_JUMPING_H

#define jumpingTilesLen 10240
extern const unsigned int jumpingTiles[2560];

#define jumpingPalLen 32
extern const unsigned short jumpingPal[16];

#endif // GRIT_JUMPING_H

//}}BLOCK(jumping)
