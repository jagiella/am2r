
//{{BLOCK(morphball)

//======================================================================
//
//	morphball, 512x64@4, 
//	+ palette 16 entries, not compressed
//	+ 512 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 16384 = 16416
//
//	Time-stamp: 2019-11-24, 22:57:34
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_MORPHBALL_H
#define GRIT_MORPHBALL_H

#define morphballTilesLen 16384
extern const unsigned int morphballTiles[4096];

#define morphballPalLen 32
extern const unsigned short morphballPal[16];

#endif // GRIT_MORPHBALL_H

//}}BLOCK(morphball)
