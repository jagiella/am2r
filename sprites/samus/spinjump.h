
//{{BLOCK(spinjump)

//======================================================================
//
//	spinjump, 512x64@4, 
//	+ palette 16 entries, not compressed
//	+ 512 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 16384 = 16416
//
//	Time-stamp: 2019-11-24, 22:58:07
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SPINJUMP_H
#define GRIT_SPINJUMP_H

#define spinjumpTilesLen 16384
extern const unsigned int spinjumpTiles[4096];

#define spinjumpPalLen 32
extern const unsigned short spinjumpPal[16];

#endif // GRIT_SPINJUMP_H

//}}BLOCK(spinjump)
