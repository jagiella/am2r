
//{{BLOCK(samus_running_sheet)

//======================================================================
//
//	samus_running_sheet, 640x64@4, 
//	+ palette 16 entries, not compressed
//	+ 640 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 20480 = 20512
//
//	Time-stamp: 2019-11-24, 22:57:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SAMUS_RUNNING_SHEET_H
#define GRIT_SAMUS_RUNNING_SHEET_H

#define samus_running_sheetTilesLen 20480
extern const unsigned int samus_running_sheetTiles[5120];

#define samus_running_sheetPalLen 32
extern const unsigned short samus_running_sheetPal[16];

#endif // GRIT_SAMUS_RUNNING_SHEET_H

//}}BLOCK(samus_running_sheet)
