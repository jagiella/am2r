
//{{BLOCK(gripping)

//======================================================================
//
//	gripping, 512x64@4, 
//	+ palette 16 entries, not compressed
//	+ 512 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 16384 = 16416
//
//	Time-stamp: 2019-11-24, 23:51:08
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_GRIPPING_H
#define GRIT_GRIPPING_H

#define grippingTilesLen 16384
extern const unsigned int grippingTiles[4096];

#define grippingPalLen 32
extern const unsigned short grippingPal[16];

#endif // GRIT_GRIPPING_H

//}}BLOCK(gripping)
