
//{{BLOCK(samus_standing_sheet)

//======================================================================
//
//	samus_standing_sheet, 256x64@4, 
//	+ palette 16 entries, not compressed
//	+ 256 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2019-11-24, 22:57:57
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SAMUS_STANDING_SHEET_H
#define GRIT_SAMUS_STANDING_SHEET_H

#define samus_standing_sheetTilesLen 8192
extern const unsigned int samus_standing_sheetTiles[2048];

#define samus_standing_sheetPalLen 32
extern const unsigned short samus_standing_sheetPal[16];

#endif // GRIT_SAMUS_STANDING_SHEET_H

//}}BLOCK(samus_standing_sheet)
