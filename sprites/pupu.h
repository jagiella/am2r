
//{{BLOCK(pupu)

//======================================================================
//
//	pupu, 32x32@4, 
//	+ palette 16 entries, not compressed
//	+ 16 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2019-10-26, 19:18:46
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PUPU_H
#define GRIT_PUPU_H

#define pupuTilesLen 512
extern const unsigned int pupuTiles[128];

#define pupuPalLen 32
extern const unsigned short pupuPal[16];

#endif // GRIT_PUPU_H

//}}BLOCK(pupu)
