
//{{BLOCK(savepoint)

//======================================================================
//
//	savepoint, 32x64@4, 
//	+ palette 16 entries, not compressed
//	+ 32 tiles not compressed
//	Total size: 32 + 1024 = 1056
//
//	Time-stamp: 2019-11-11, 12:45:29
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SAVEPOINT_H
#define GRIT_SAVEPOINT_H

#define savepointTilesLen 1024
extern const unsigned int savepointTiles[256];

#define savepointPalLen 32
extern const unsigned short savepointPal[16];

#endif // GRIT_SAVEPOINT_H

//}}BLOCK(savepoint)
