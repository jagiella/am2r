
//{{BLOCK(beam)

//======================================================================
//
//	beam, 96x32@4, 
//	+ palette 16 entries, not compressed
//	+ 48 tiles Metatiled by 2x2 not compressed
//	Total size: 32 + 1536 = 1568
//
//	Time-stamp: 2019-11-09, 00:01:35
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BEAM_H
#define GRIT_BEAM_H

#define beamTilesLen 1536
extern const unsigned int beamTiles[384];

#define beamPalLen 32
extern const unsigned short beamPal[16];

#endif // GRIT_BEAM_H

//}}BLOCK(beam)
