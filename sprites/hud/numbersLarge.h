
//{{BLOCK(numbersLarge)

//======================================================================
//
//	numbersLarge, 160x16@4, 
//	+ palette 16 entries, not compressed
//	+ 40 tiles Metatiled by 2x2 not compressed
//	Total size: 32 + 1280 = 1312
//
//	Time-stamp: 2019-12-07, 12:55:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_NUMBERSLARGE_H
#define GRIT_NUMBERSLARGE_H

#define numbersLargeTilesLen 1280
extern const unsigned int numbersLargeTiles[320];

#define numbersLargePalLen 32
extern const unsigned short numbersLargePal[16];

#endif // GRIT_NUMBERSLARGE_H

//}}BLOCK(numbersLarge)
