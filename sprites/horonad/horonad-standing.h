
//{{BLOCK(horonad_standing)

//======================================================================
//
//	horonad_standing, 192x32@4, 
//	+ palette 16 entries, not compressed
//	+ 96 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 3072 = 3104
//
//	Time-stamp: 2019-10-21, 12:24:13
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_HORONAD_STANDING_H
#define GRIT_HORONAD_STANDING_H

#define horonad_standingTilesLen 3072
extern const unsigned int horonad_standingTiles[768];

#define horonad_standingPalLen 32
extern const unsigned short horonad_standingPal[16];

#endif // GRIT_HORONAD_STANDING_H

//}}BLOCK(horonad_standing)
