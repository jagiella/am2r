
//{{BLOCK(gunship)

//======================================================================
//
//	gunship, 192x64@4, 
//	+ palette 16 entries, not compressed
//	+ 192 tiles Metatiled by 8x8 not compressed
//	Total size: 32 + 6144 = 6176
//
//	Time-stamp: 2019-11-06, 11:43:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_GUNSHIP_H
#define GRIT_GUNSHIP_H

#define gunshipTilesLen 6144
extern const unsigned int gunshipTiles[1536];

#define gunshipPalLen 32
extern const unsigned short gunshipPal[16];

#endif // GRIT_GUNSHIP_H

//}}BLOCK(gunship)
