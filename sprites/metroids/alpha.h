
//{{BLOCK(alpha)

//======================================================================
//
//	alpha, 64x64@4, 
//	+ palette 16 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2019-11-29, 01:29:15
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_ALPHA_H
#define GRIT_ALPHA_H

#define alphaTilesLen 2048
extern const unsigned int alphaTiles[512];

#define alphaPalLen 32
extern const unsigned short alphaPal[16];

#endif // GRIT_ALPHA_H

//}}BLOCK(alpha)
