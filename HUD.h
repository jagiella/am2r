/*
 * HUD.h
 *
 *  Created on: 07.12.2019
 *      Author: nick
 */

#ifndef HUD_H_
#define HUD_H_


#include "ObjectHandler.h"
#include "Samus.h"
#include "Tilemap.h"

class HUD: public Object{

	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	static SpriteEngine *spriteEngine;

	Player *_player;
public:
	static void init( SpriteEngine *spriteEngine);

	HUD( Player *player);
	int x(){ return 0;}; // not needed
	int y(){ return 0;}; // not needed
	void update( Tilemap *tilemap){}; // not needed
	void updateScreen( Tilemap *tilemap);
};


#endif /* HUD_H_ */
