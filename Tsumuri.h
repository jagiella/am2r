/*
 * Tsumuri.h
 *
 *  Created on: 21.10.2019
 *      Author: nick
 */

#ifndef TSUMURI_H_
#define TSUMURI_H_

#include "SpriteEngine.h"
#include "ObjectHandler.h"

class Tsumuri: public Object{
		// STATIC
private:
	static signed int sTid;
	static signed int sPb;
public:
	static void init( SpriteEngine *spriteEngine);

	// NON_STATIC
	int tid = 14*8*8;
	int pb = 1;
	//OBJ_ATTR *sprite;
	ObjectHandler *objHandler;
	int objID;
	int _x=0,_y=0;
	int bb[2][2] = {{0,0}, {32,32}};
	//int direction = RIGHT;
	int dx = 1, dy = 0;
public:
	Tsumuri( int x, int y): _x(x), _y(y){


		objID = ObjectHandler::instance().add();
		//sprite = &obj_buffer[objID];
		obj_set_attr( ObjectHandler::instance().get( objID),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x32,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(sPb) | sTid);
					//obj_set_pos(sprite, x*16, y*16);
		obj_set_pos( ObjectHandler::instance().get( objID), _x, _y);
	}

	int x(){
		return _x;
	}
	int y(){
		return _y;
	}

	bool onGround( Tilemap* room){
		return room->collisionDetection( _x+bb[0][0], _y+bb[0][1]+1, _x+bb[1][0], _y+bb[1][1]+1);
	}
	bool onCeiling( Tilemap* room){
		return room->collisionDetection( _x+bb[0][0], _y+bb[0][1]-1, _x+bb[1][0], _y+bb[1][1]-1);
	}
	bool onWall( Tilemap* room){
		return room->collisionDetection( _x+bb[0][0]-1, _y+bb[0][1], _x+bb[1][0]+1, _y+bb[1][1]);
	}

	void turnRight( int dx, int dy, int& ndx, int& ndy){
		ndx = 0;
		ndy = 0;

		if(dx == 0)
			ndx = -dy;
		else
			ndy = dx;
	}

	void turnLeft( int dx, int dy, int& ndx, int& ndy){
		ndx = 0;
		ndy = 0;

		if(dx == 0)
			ndx = +dy;
		else
			ndy = -dx;
	}



	void update( Tilemap* room){
		int ndx, ndy;
		turnRight(dx,dy,ndx,ndy);

		if( !room->collisionDetection( _x+ndx+bb[0][0], _y+ndy+bb[0][1], _x+ndx+bb[1][0], _y+ndy+bb[1][1])){
			// fall to floor
			//while(!onGround(room))
			for( int i=0; i<4 and !onGround(room); i++){
				// always fall to floor
				_y++;
			}
			dx = 1;
			dy = 0;
		}

		if( !room->collisionDetection( _x+dx+bb[0][0], _y+dy+bb[0][1], _x+dx+bb[1][0], _y+dy+bb[1][1])){
			// move forward
			_x += dx;
			_y += dy;


			if( !room->collisionDetection( _x+ndx+bb[0][0], _y+ndy+bb[0][1], _x+ndx+bb[1][0], _y+ndy+bb[1][1])){
				// turn right
				dx=ndx;
				dy=ndy;
				_x += dx;
				_y += dy;
			}
		}else{
			// turn left
			turnLeft(dx,dy,ndx,ndy);
			dx=ndx;
			dy=ndy;
		}
	}

	void updateScreen( Tilemap *room){
		if( _x+bb[1][0] < room->x or _x+bb[0][0] > room->x+240 or _y+bb[1][1] < room->y or _y+bb[0][1] > room->y+160){
			// not on screen

		}else
			obj_set_pos(ObjectHandler::instance().get( objID), _x-room->x, _y-room->y);
	}


	bool collision( int x0, int y0, int x1, int y1) {
		return false;
	}

	int objCount() {return 1;};
	OBJ_ATTR obj( int index) {
		return *ObjectHandler::instance().get( objID);
	}
};



#endif /* TSUMURI_H_ */
