/*
 * Tsumuri.cpp
 *
 *  Created on: 21.10.2019
 *      Author: nick
 */



#include "Tsumuri.h"

#include "SpriteEngine.h"
#include "sprites/tsumuri/tsumuri.h"

void Tsumuri::init( SpriteEngine *spriteEngine){
	sTid = spriteEngine->addTiles( tsumuriTiles, tsumuriTilesLen);
	sPb  = spriteEngine->addPalette( tsumuriPal, tsumuriPalLen);
}

signed int Tsumuri::sTid = 0;
signed int Tsumuri::sPb = 0;
