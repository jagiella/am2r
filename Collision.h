/*
 * Collision.h
 *
 *  Created on: 06.11.2019
 *      Author: nick
 */

#ifndef COLLISION_H_
#define COLLISION_H_


struct Point
{
    int x;
    int y;
};

int testPointInPolygon( int *p, int **polygon, int polygonLen);
bool testRectangleCollision( int rect1_x0, int rect1_y0, int rect1_x1, int rect1_y1, int rect2_x0, int rect2_y0, int rect2_x1, int rect2_y1);


#endif /* COLLISION_H_ */
