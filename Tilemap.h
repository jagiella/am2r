/*
 * Tilemap.h
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#ifndef TILEMAP_H_
#define TILEMAP_H_

extern "C" {
	#include "tonc_types.h"
	#include "tonc_memmap.h"
	#include "tonc_oam.h"
}

#include <math.h>
#include <string.h>
#define MINMAX( a,x,b) (x<=a?a:(x<=b?x:b))
#define MODULO( x,a)   ((x+a) % (a))

class Tilemap{
protected:
	const unsigned short* map;
	int map_w, map_h;
//	int leftMapID = 0;
//	int rightMapID = 1;
	int sbIDs[2][2];//  = {{4,5},{6,7}};
	int mapIDs[2][2] = {{-10,-10},{-10,-10}};
	int left = 0;
	//int backgroundID;
	vu16* reg_x;
	vu16* reg_y;
	bool wrapX = false;
	bool wrapY = false;
	int moveX = 0;
	int offsetX =0;
	int moveDelay = 5;
	int moveCounter = 0;

public:
	int x=0, y=0;
public:
	Tilemap( const unsigned short* map, const int map_w, const int map_h, int backgroundID, int screenblockIndex):
		map(map),
		map_w(map_w),
		map_h(map_h)//,
		//backgroundID(backgroundID)//,
		//_screenblockIndex(screenblockIndex)
	{
		//loadScreenblock( map, mapIDs[0], sbIDs[0]);
		//loadScreenblock( map, mapIDs[1], sbIDs[1]);
		//this->screenblockIndex = screenblockIndex;
		if( screenblockIndex<=32-4){
//			int sbCount = 0;
//			for( int i=0; i<2 and i<map_w; i++){
//				for( int j=0; j<2 and j<map_h; j++){
//					sbIDs[i][j] = screenblockIndex + sbCount;
//					sbCount++;
//				}
//			}
			sbIDs[0][0] = screenblockIndex + 0;
			sbIDs[0][1] = screenblockIndex + 1;
			sbIDs[1][0] = screenblockIndex + 2;
			sbIDs[1][1] = screenblockIndex + 3;
		}
//		if( backgroundID<4){
//			sbIDs[0][0] = backgroundID*8+4;
//			sbIDs[0][1] = backgroundID*8+5;
//			sbIDs[1][0] = backgroundID*8+6;
//			sbIDs[1][1] = backgroundID*8+7;
//		}
		switch( backgroundID){
		case 0:
//			sbIDs[0][0] = 4;
//			sbIDs[0][1] = 5;
//			sbIDs[1][0] = 6;
//			sbIDs[1][1] = 7;
			reg_x = &REG_BG0HOFS;
			reg_y = &REG_BG0VOFS;
			break;
		case 1:
//			sbIDs[0][0] = 12;
//			sbIDs[0][1] = 13;
//			sbIDs[1][0] = 14;
//			sbIDs[1][1] = 15;
			reg_x = &REG_BG1HOFS;
			reg_y = &REG_BG1VOFS;
			break;
		case 2:
//			sbIDs[0][0] = 20;
//			sbIDs[0][1] = 21;
//			sbIDs[1][0] = 22;
//			sbIDs[1][1] = 23;
			reg_x = &REG_BG2HOFS;
			reg_y = &REG_BG2VOFS;
			break;
		case 3:
//			sbIDs[0][0] = 28;
//			sbIDs[0][1] = 29;
//			sbIDs[1][0] = 30;
//			sbIDs[1][1] = 31;
			reg_x = &REG_BG3HOFS;
			reg_y = &REG_BG3VOFS;
			break;
		//default:
		}

	};

	int screenblockIndex(){
		return sbIDs[0][0];
	}

	/*void update(int x, int y){
		int rightScreenCorner_x = x+240; // offset + screen width
		int rightMapID = mapIDs[1-left];
		if( rightScreenCorner_x > (rightMapID+1)*8*32 ){
			// extend right
			mapIDs[left] = rightMapID + 1;
			loadScreenblock( map, mapIDs[left], sbIDs[left]);
			left = 1-left;
		}

		int leftScreenCorner_x = x; // offset
		int leftMapID = mapIDs[left];
		if( leftScreenCorner_x < leftMapID*8*32 ){
			// extend left
			mapIDs[1-left] = leftMapID - 1;
			loadScreenblock( map, mapIDs[1-left], sbIDs[1-left]);
			left = 1-left;
		}
	}*/

	void loadScreenblock( const unsigned short* map, int mapID, int screenblockID){
		//emcpy( se_mem[screenblockID], &map[ mapID*1024], 2048);
		memcpy16( se_mem[screenblockID], &map[ mapID*1024], 1024);
	}

	void setWrapX( bool wrapX){
		this->wrapX = wrapX;
	}
	void setWrapY( bool wrapY){
		this->wrapY = wrapY;
	}
	void setMoveX( int pixels){
		this->moveX = pixels;
	}

	void update(int xp, int yp){

		int x1;
		int y1;

		if( wrapX){
			x  = MODULO( xp, map_w*8*32);
			x1 = MODULO( xp+240, map_w*8*32);
		}
		else{
			x  = MINMAX(0,xp-120,map_w*8*32-240);
			x1 = x+240;
		}

		if( wrapY){
			y  = MODULO( yp    , map_h*8*32);
			y1 = MODULO( yp+160, map_h*8*32);
		}else{
			y  = MINMAX(0,yp- 80,map_h*8*32-160);
			y1 = y+160;
		}

		moveCounter++;
		if(moveCounter % moveDelay == 0)
			offsetX = ( offsetX+moveX)%(8*32);
		x += offsetX;
		x1 += offsetX;

		int X[4] = {x ,x ,x1,x1};
		int Y[4] = {y ,y1,y ,y1};
		//int X[2] = {x0,x1};

		for( int i=0; i<4; i++){
			int mbc[2] = {X[i]/(8*32), Y[i]/(8*32)};// map block coordinates
			//int mbc[1] = {X[i]/(8*32)};// map block coordinates
			if( mbc[0]>=0 and mbc[0] < map_w and mbc[1]>=0 and mbc[1] < map_h){
				int mbID = mbc[0] + mbc[1]*map_w;
				//int index[1] = {mbc[0]%2};
				int index[2] = {mbc[0]%2, mbc[1]%2};
				//if( mapIDs[index[1]][index[0]] != mbID)
				{
					mapIDs[index[1]][index[0]] = mbID;
					loadScreenblock( map, mbID, sbIDs[index[1]][index[0]]);
				}
			}
		}


		//REG_BG0HOFS = x;
		//REG_BG0VOFS = y;
		*reg_x = x;
		*reg_y = y;
	}


	unsigned short getTileIndex( int x, int y);

	int position2tile( int pos);

	bool collisionDetection( int x0, int y0, int x1, int y1);

	bool inMap( int x, int y){
		return x >= 0 and x < map_w*32*8 and y >= 0 and y < map_h*32*8;
	}
};


#endif /* TILEMAP_H_ */
