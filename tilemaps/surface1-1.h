
//{{BLOCK(surface1_1)

//======================================================================
//
//	surface1_1, 1024x512@4, 
//	+ palette 16 entries, not compressed
//	+ 88 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x64 
//	Total size: 32 + 2816 + 16384 = 19232
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE1_1_H
#define GRIT_SURFACE1_1_H

#define surface1_1TilesLen 2816
extern const unsigned int surface1_1Tiles[704];

#define surface1_1MapLen 16384
extern const unsigned short surface1_1Map[8192];

#define surface1_1PalLen 32
extern const unsigned short surface1_1Pal[16];

#endif // GRIT_SURFACE1_1_H

//}}BLOCK(surface1_1)
