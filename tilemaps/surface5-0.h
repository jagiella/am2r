
//{{BLOCK(surface5_0)

//======================================================================
//
//	surface5_0, 768x512@4, 
//	+ palette 16 entries, not compressed
//	+ 162 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x64 
//	Total size: 32 + 5184 + 12288 = 17504
//
//	Time-stamp: 2020-02-04, 22:21:55
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE5_0_H
#define GRIT_SURFACE5_0_H

#define surface5_0TilesLen 5184
extern const unsigned int surface5_0Tiles[1296];

#define surface5_0MapLen 12288
extern const unsigned short surface5_0Map[6144];

#define surface5_0PalLen 32
extern const unsigned short surface5_0Pal[16];

#endif // GRIT_SURFACE5_0_H

//}}BLOCK(surface5_0)
