int roomOffsets[10][2] = {
  {0, 0},
  {1024, 256},
  {2304, 512},
  {2048, 1536},
  {1280, 1536},
  {1024, 1536},
  {0, 2304},
  {-256, 2304},
  {-1280, 2304},
  {-2048, 2304},
};
Transition surface1Tra[] = {
  { {{1016, 256}, {16, 256}}, 1, 1, UP},
};
Transition surface2Tra[] = {
  { {{-8, 0}, {16, 256}}, 0, 1, UP},
  { {{1272, 256}, {16, 256}}, 2, 1, UP},
};
Transition surface4Tra[] = {
  { {{-8, 0}, {16, 256}}, 1, 1, UP},
  { {{0, 1016}, {256, 16}}, 3, 1, UP},
};
Transition surface5Tra[] = {
  { {{256, -8}, {256, 16}}, 2, 1, UP},
  { {{-8, 0}, {16, 512}}, 4, 1, UP},
};
Transition surface6Tra[] = {
  { {{760, 0}, {16, 512}}, 3, 1, UP},
  { {{-8, 0}, {16, 512}}, 5, 1, UP},
};
Transition surface7Tra[] = {
  { {{248, 0}, {16, 512}}, 4, 1, UP},
  { {{-8, 768}, {16, 256}}, 6, 1, UP},
};
Transition surface8Tra[] = {
  { {{1016, 0}, {16, 256}}, 5, 1, UP},
  { {{-8, 0}, {16, 256}}, 7, 1, UP},
};
Transition surface9Tra[] = {
  { {{248, 0}, {16, 256}}, 6, 1, UP},
  { {{-8, 0}, {16, 256}}, 8, 1, UP},
};
Transition surface10Tra[] = {
  { {{1016, 0}, {16, 256}}, 7, 1, UP},
  { {{-8, 0}, {16, 256}}, 9, 1, UP},
};
Transition surface11Tra[] = {
  { {{760, 0}, {16, 256}}, 8, 1, UP},
};
Transition *transitions[10] = {surface1Tra, surface2Tra, surface4Tra, surface5Tra, surface6Tra, surface7Tra, surface8Tra, surface9Tra, surface10Tra, surface11Tra, };
int transitionLen[10] = {1, 2, 2, 2, 2, 2, 2, 2, 2, 1, };
int mapBlocks[10][2] = { {4, 2},{5, 2},{1, 4},{3, 2},{3, 2},{1, 4},{4, 1},{1, 1},{4, 2},{3, 1},};
const unsigned short* colMap[10] = { surface1Map,surface2Map,surface4Map,surface5Map,surface6Map,surface7Map,surface8Map,surface9Map,surface10Map,surface11Map,};
int colMapLen[10] = { surface1MapLen,surface2MapLen,surface4MapLen,surface5MapLen,surface6MapLen,surface7MapLen,surface8MapLen,surface9MapLen,surface10MapLen,surface11MapLen,};
const unsigned int* bg0Tiles[10] = { surface1_0Tiles,surface2_0Tiles,surface4_0Tiles,surface5_0Tiles,surface6_0Tiles,surface7_0Tiles,surface8_0Tiles,surface9_0Tiles,surface10_0Tiles,surface11_0Tiles,};
int bg0TilesLen[10] = { surface1_0TilesLen,surface2_0TilesLen,surface4_0TilesLen,surface5_0TilesLen,surface6_0TilesLen,surface7_0TilesLen,surface8_0TilesLen,surface9_0TilesLen,surface10_0TilesLen,surface11_0TilesLen,};
const unsigned short* bg0Pal[10] = { surface1_0Pal,surface2_0Pal,surface4_0Pal,surface5_0Pal,surface6_0Pal,surface7_0Pal,surface8_0Pal,surface9_0Pal,surface10_0Pal,surface11_0Pal,};
int bg0PalLen[10] = { surface1_0PalLen,surface2_0PalLen,surface4_0PalLen,surface5_0PalLen,surface6_0PalLen,surface7_0PalLen,surface8_0PalLen,surface9_0PalLen,surface10_0PalLen,surface11_0PalLen,};
const unsigned short* bg0Map[10] = { surface1_0Map,surface2_0Map,surface4_0Map,surface5_0Map,surface6_0Map,surface7_0Map,surface8_0Map,surface9_0Map,surface10_0Map,surface11_0Map,};
int bg0MapLen[10] = { surface1_0MapLen,surface2_0MapLen,surface4_0MapLen,surface5_0MapLen,surface6_0MapLen,surface7_0MapLen,surface8_0MapLen,surface9_0MapLen,surface10_0MapLen,surface11_0MapLen,};
const unsigned int* bg1Tiles[10] = { surface1_1Tiles,surface2_1Tiles,surface4_1Tiles,surface5_1Tiles,surface6_1Tiles,surface7_1Tiles,surface8_1Tiles,surface9_1Tiles,surface10_1Tiles,surface11_1Tiles,};
int bg1TilesLen[10] = { surface1_1TilesLen,surface2_1TilesLen,surface4_1TilesLen,surface5_1TilesLen,surface6_1TilesLen,surface7_1TilesLen,surface8_1TilesLen,surface9_1TilesLen,surface10_1TilesLen,surface11_1TilesLen,};
const unsigned short* bg1Pal[10] = { surface1_1Pal,surface2_1Pal,surface4_1Pal,surface5_1Pal,surface6_1Pal,surface7_1Pal,surface8_1Pal,surface9_1Pal,surface10_1Pal,surface11_1Pal,};
int bg1PalLen[10] = { surface1_1PalLen,surface2_1PalLen,surface4_1PalLen,surface5_1PalLen,surface6_1PalLen,surface7_1PalLen,surface8_1PalLen,surface9_1PalLen,surface10_1PalLen,surface11_1PalLen,};
const unsigned short* bg1Map[10] = { surface1_1Map,surface2_1Map,surface4_1Map,surface5_1Map,surface6_1Map,surface7_1Map,surface8_1Map,surface9_1Map,surface10_1Map,surface11_1Map,};
int bg1MapLen[10] = { surface1_1MapLen,surface2_1MapLen,surface4_1MapLen,surface5_1MapLen,surface6_1MapLen,surface7_1MapLen,surface8_1MapLen,surface9_1MapLen,surface10_1MapLen,surface11_1MapLen,};
