
//{{BLOCK(background2)

//======================================================================
//
//	background2, 512x512@4, 
//	+ palette 16 entries, not compressed
//	+ 251 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 64x64 
//	Total size: 32 + 8032 + 8192 = 16256
//
//	Time-stamp: 2019-11-28, 12:02:50
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BACKGROUND2_H
#define GRIT_BACKGROUND2_H

#define background2TilesLen 8032
extern const unsigned int background2Tiles[2008];

#define background2MapLen 8192
extern const unsigned short background2Map[4096];

#define background2PalLen 32
extern const unsigned short background2Pal[16];

#endif // GRIT_BACKGROUND2_H

//}}BLOCK(background2)
