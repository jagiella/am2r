
//{{BLOCK(surface7_1)

//======================================================================
//
//	surface7_1, 256x1024@4, 
//	+ palette 16 entries, not compressed
//	+ 166 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x128 
//	Total size: 32 + 5312 + 8192 = 13536
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE7_1_H
#define GRIT_SURFACE7_1_H

#define surface7_1TilesLen 5312
extern const unsigned int surface7_1Tiles[1328];

#define surface7_1MapLen 8192
extern const unsigned short surface7_1Map[4096];

#define surface7_1PalLen 32
extern const unsigned short surface7_1Pal[16];

#endif // GRIT_SURFACE7_1_H

//}}BLOCK(surface7_1)
