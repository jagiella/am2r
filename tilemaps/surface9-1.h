
//{{BLOCK(surface9_1)

//======================================================================
//
//	surface9_1, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 46 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x32 
//	Total size: 32 + 1472 + 2048 = 3552
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE9_1_H
#define GRIT_SURFACE9_1_H

#define surface9_1TilesLen 1472
extern const unsigned int surface9_1Tiles[368];

#define surface9_1MapLen 2048
extern const unsigned short surface9_1Map[1024];

#define surface9_1PalLen 32
extern const unsigned short surface9_1Pal[16];

#endif // GRIT_SURFACE9_1_H

//}}BLOCK(surface9_1)
