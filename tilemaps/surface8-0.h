
//{{BLOCK(surface8_0)

//======================================================================
//
//	surface8_0, 1024x256@4, 
//	+ palette 16 entries, not compressed
//	+ 156 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x32 
//	Total size: 32 + 4992 + 8192 = 13216
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE8_0_H
#define GRIT_SURFACE8_0_H

#define surface8_0TilesLen 4992
extern const unsigned int surface8_0Tiles[1248];

#define surface8_0MapLen 8192
extern const unsigned short surface8_0Map[4096];

#define surface8_0PalLen 32
extern const unsigned short surface8_0Pal[16];

#endif // GRIT_SURFACE8_0_H

//}}BLOCK(surface8_0)
