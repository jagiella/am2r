
//{{BLOCK(surface11_1)

//======================================================================
//
//	surface11_1, 768x256@4, 
//	+ palette 16 entries, not compressed
//	+ 106 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x32 
//	Total size: 32 + 3392 + 6144 = 9568
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE11_1_H
#define GRIT_SURFACE11_1_H

#define surface11_1TilesLen 3392
extern const unsigned int surface11_1Tiles[848];

#define surface11_1MapLen 6144
extern const unsigned short surface11_1Map[3072];

#define surface11_1PalLen 32
extern const unsigned short surface11_1Pal[16];

#endif // GRIT_SURFACE11_1_H

//}}BLOCK(surface11_1)
