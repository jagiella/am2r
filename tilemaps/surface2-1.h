
//{{BLOCK(surface2_1)

//======================================================================
//
//	surface2_1, 1280x512@4, 
//	+ palette 16 entries, not compressed
//	+ 127 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 160x64 
//	Total size: 32 + 4064 + 20480 = 24576
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE2_1_H
#define GRIT_SURFACE2_1_H

#define surface2_1TilesLen 4064
extern const unsigned int surface2_1Tiles[1016];

#define surface2_1MapLen 20480
extern const unsigned short surface2_1Map[10240];

#define surface2_1PalLen 32
extern const unsigned short surface2_1Pal[16];

#endif // GRIT_SURFACE2_1_H

//}}BLOCK(surface2_1)
