
//{{BLOCK(background1)

//======================================================================
//
//	background1, 512x512@4, 
//	+ palette 16 entries, not compressed
//	+ 472 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 64x64 
//	Total size: 32 + 15104 + 8192 = 23328
//
//	Time-stamp: 2019-10-31, 17:29:11
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BACKGROUND1_H
#define GRIT_BACKGROUND1_H

#define background1TilesLen 15104
extern const unsigned int background1Tiles[3776];

#define background1MapLen 8192
extern const unsigned short background1Map[4096];

#define background1PalLen 32
extern const unsigned short background1Pal[16];

#endif // GRIT_BACKGROUND1_H

//}}BLOCK(background1)
