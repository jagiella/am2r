
//{{BLOCK(surface1)

//======================================================================
//
//	surface1, 1024x512@4, 
//	+ palette 16 entries, not compressed
//	+ 4 tiles (t|f|p reduced) not compressed
//	+ regular map (in SBBs), lz77 compressed, 128x64 
//	Total size: 32 + 128 + 1960 = 2120
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE1_H
#define GRIT_SURFACE1_H

#define surface1TilesLen 128
extern const unsigned int surface1Tiles[32];

#define surface1MapLen 1960
extern const unsigned short surface1Map[980];

#define surface1PalLen 32
extern const unsigned short surface1Pal[16];

#endif // GRIT_SURFACE1_H

//}}BLOCK(surface1)
