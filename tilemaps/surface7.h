
//{{BLOCK(surface7)

//======================================================================
//
//	surface7, 256x1024@4, 
//	+ regular map (in SBBs), lz77 compressed, 32x128 
//	External tile file: ../tilesets/collisionTiles.png.
//	Total size: 988 = 988
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE7_H
#define GRIT_SURFACE7_H

#define surface7MapLen 988
extern const unsigned short surface7Map[494];

#endif // GRIT_SURFACE7_H

//}}BLOCK(surface7)
