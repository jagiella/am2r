
//{{BLOCK(surface4_0)

//======================================================================
//
//	surface4_0, 256x1024@4, 
//	+ palette 16 entries, not compressed
//	+ 259 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x128 
//	Total size: 32 + 8288 + 8192 = 16512
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE4_0_H
#define GRIT_SURFACE4_0_H

#define surface4_0TilesLen 8288
extern const unsigned int surface4_0Tiles[2072];

#define surface4_0MapLen 8192
extern const unsigned short surface4_0Map[4096];

#define surface4_0PalLen 32
extern const unsigned short surface4_0Pal[16];

#endif // GRIT_SURFACE4_0_H

//}}BLOCK(surface4_0)
