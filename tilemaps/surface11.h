
//{{BLOCK(surface11)

//======================================================================
//
//	surface11, 768x256@4, 
//	+ regular map (in SBBs), lz77 compressed, 96x32 
//	External tile file: ../tilesets/collisionTiles.png.
//	Total size: 748 = 748
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE11_H
#define GRIT_SURFACE11_H

#define surface11MapLen 748
extern const unsigned short surface11Map[374];

#endif // GRIT_SURFACE11_H

//}}BLOCK(surface11)
