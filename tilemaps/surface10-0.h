
//{{BLOCK(surface10_0)

//======================================================================
//
//	surface10_0, 1024x512@4, 
//	+ palette 16 entries, not compressed
//	+ 170 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x64 
//	Total size: 32 + 5440 + 16384 = 21856
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE10_0_H
#define GRIT_SURFACE10_0_H

#define surface10_0TilesLen 5440
extern const unsigned int surface10_0Tiles[1360];

#define surface10_0MapLen 16384
extern const unsigned short surface10_0Map[8192];

#define surface10_0PalLen 32
extern const unsigned short surface10_0Pal[16];

#endif // GRIT_SURFACE10_0_H

//}}BLOCK(surface10_0)
