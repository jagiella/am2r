
//{{BLOCK(surface2_0)

//======================================================================
//
//	surface2_0, 1280x512@4, 
//	+ palette 16 entries, not compressed
//	+ 305 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 160x64 
//	Total size: 32 + 9760 + 20480 = 30272
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE2_0_H
#define GRIT_SURFACE2_0_H

#define surface2_0TilesLen 9760
extern const unsigned int surface2_0Tiles[2440];

#define surface2_0MapLen 20480
extern const unsigned short surface2_0Map[10240];

#define surface2_0PalLen 32
extern const unsigned short surface2_0Pal[16];

#endif // GRIT_SURFACE2_0_H

//}}BLOCK(surface2_0)
