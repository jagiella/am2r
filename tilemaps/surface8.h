
//{{BLOCK(surface8)

//======================================================================
//
//	surface8, 1024x256@4, 
//	+ regular map (in SBBs), lz77 compressed, 128x32 
//	External tile file: ../tilesets/collisionTiles.png.
//	Total size: 984 = 984
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE8_H
#define GRIT_SURFACE8_H

#define surface8MapLen 984
extern const unsigned short surface8Map[492];

#endif // GRIT_SURFACE8_H

//}}BLOCK(surface8)
