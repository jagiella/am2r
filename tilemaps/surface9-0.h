
//{{BLOCK(surface9_0)

//======================================================================
//
//	surface9_0, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 128 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x32 
//	Total size: 32 + 4096 + 2048 = 6176
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE9_0_H
#define GRIT_SURFACE9_0_H

#define surface9_0TilesLen 4096
extern const unsigned int surface9_0Tiles[1024];

#define surface9_0MapLen 2048
extern const unsigned short surface9_0Map[1024];

#define surface9_0PalLen 32
extern const unsigned short surface9_0Pal[16];

#endif // GRIT_SURFACE9_0_H

//}}BLOCK(surface9_0)
