
//{{BLOCK(surface11_0)

//======================================================================
//
//	surface11_0, 768x256@4, 
//	+ palette 16 entries, not compressed
//	+ 172 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x32 
//	Total size: 32 + 5504 + 6144 = 11680
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE11_0_H
#define GRIT_SURFACE11_0_H

#define surface11_0TilesLen 5504
extern const unsigned int surface11_0Tiles[1376];

#define surface11_0MapLen 6144
extern const unsigned short surface11_0Map[3072];

#define surface11_0PalLen 32
extern const unsigned short surface11_0Pal[16];

#endif // GRIT_SURFACE11_0_H

//}}BLOCK(surface11_0)
