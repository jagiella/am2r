
//{{BLOCK(surface7_0)

//======================================================================
//
//	surface7_0, 256x1024@4, 
//	+ palette 16 entries, not compressed
//	+ 167 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x128 
//	Total size: 32 + 5344 + 8192 = 13568
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE7_0_H
#define GRIT_SURFACE7_0_H

#define surface7_0TilesLen 5344
extern const unsigned int surface7_0Tiles[1336];

#define surface7_0MapLen 8192
extern const unsigned short surface7_0Map[4096];

#define surface7_0PalLen 32
extern const unsigned short surface7_0Pal[16];

#endif // GRIT_SURFACE7_0_H

//}}BLOCK(surface7_0)
