
//{{BLOCK(surface5_1)

//======================================================================
//
//	surface5_1, 768x512@4, 
//	+ palette 16 entries, not compressed
//	+ 180 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x64 
//	Total size: 32 + 5760 + 12288 = 18080
//
//	Time-stamp: 2020-02-04, 22:21:55
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE5_1_H
#define GRIT_SURFACE5_1_H

#define surface5_1TilesLen 5760
extern const unsigned int surface5_1Tiles[1440];

#define surface5_1MapLen 12288
extern const unsigned short surface5_1Map[6144];

#define surface5_1PalLen 32
extern const unsigned short surface5_1Pal[16];

#endif // GRIT_SURFACE5_1_H

//}}BLOCK(surface5_1)
