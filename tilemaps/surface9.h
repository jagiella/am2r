
//{{BLOCK(surface9)

//======================================================================
//
//	surface9, 256x256@4, 
//	+ regular map (in SBBs), lz77 compressed, 32x32 
//	External tile file: ../tilesets/collisionTiles.png.
//	Total size: 264 = 264
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE9_H
#define GRIT_SURFACE9_H

#define surface9MapLen 264
extern const unsigned short surface9Map[132];

#endif // GRIT_SURFACE9_H

//}}BLOCK(surface9)
