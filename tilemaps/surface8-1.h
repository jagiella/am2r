
//{{BLOCK(surface8_1)

//======================================================================
//
//	surface8_1, 1024x256@4, 
//	+ palette 16 entries, not compressed
//	+ 13 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x32 
//	Total size: 32 + 416 + 8192 = 8640
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE8_1_H
#define GRIT_SURFACE8_1_H

#define surface8_1TilesLen 416
extern const unsigned int surface8_1Tiles[104];

#define surface8_1MapLen 8192
extern const unsigned short surface8_1Map[4096];

#define surface8_1PalLen 32
extern const unsigned short surface8_1Pal[16];

#endif // GRIT_SURFACE8_1_H

//}}BLOCK(surface8_1)
