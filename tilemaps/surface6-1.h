
//{{BLOCK(surface6_1)

//======================================================================
//
//	surface6_1, 768x512@4, 
//	+ palette 16 entries, not compressed
//	+ 1 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x64 
//	Total size: 32 + 32 + 12288 = 12352
//
//	Time-stamp: 2020-02-04, 22:21:55
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE6_1_H
#define GRIT_SURFACE6_1_H

#define surface6_1TilesLen 32
extern const unsigned int surface6_1Tiles[8];

#define surface6_1MapLen 12288
extern const unsigned short surface6_1Map[6144];

#define surface6_1PalLen 32
extern const unsigned short surface6_1Pal[16];

#endif // GRIT_SURFACE6_1_H

//}}BLOCK(surface6_1)
