
//{{BLOCK(surface1_0)

//======================================================================
//
//	surface1_0, 1024x512@4, 
//	+ palette 16 entries, not compressed
//	+ 233 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x64 
//	Total size: 32 + 7456 + 16384 = 23872
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE1_0_H
#define GRIT_SURFACE1_0_H

#define surface1_0TilesLen 7456
extern const unsigned int surface1_0Tiles[1864];

#define surface1_0MapLen 16384
extern const unsigned short surface1_0Map[8192];

#define surface1_0PalLen 32
extern const unsigned short surface1_0Pal[16];

#endif // GRIT_SURFACE1_0_H

//}}BLOCK(surface1_0)
