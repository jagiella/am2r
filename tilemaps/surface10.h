
//{{BLOCK(surface10)

//======================================================================
//
//	surface10, 1024x512@4, 
//	+ regular map (in SBBs), lz77 compressed, 128x64 
//	External tile file: ../tilesets/collisionTiles.png.
//	Total size: 1956 = 1956
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE10_H
#define GRIT_SURFACE10_H

#define surface10MapLen 1956
extern const unsigned short surface10Map[978];

#endif // GRIT_SURFACE10_H

//}}BLOCK(surface10)
