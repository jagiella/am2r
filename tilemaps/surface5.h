
//{{BLOCK(surface5)

//======================================================================
//
//	surface5, 768x512@4, 
//	+ palette 16 entries, not compressed
//	+ 4 tiles (t|f|p reduced) not compressed
//	+ regular map (in SBBs), lz77 compressed, 96x64 
//	Total size: 32 + 128 + 1472 = 1632
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE5_H
#define GRIT_SURFACE5_H

#define surface5TilesLen 128
extern const unsigned int surface5Tiles[32];

#define surface5MapLen 1472
extern const unsigned short surface5Map[736];

#define surface5PalLen 32
extern const unsigned short surface5Pal[16];

#endif // GRIT_SURFACE5_H

//}}BLOCK(surface5)
