
//{{BLOCK(surface2)

//======================================================================
//
//	surface2, 1280x512@4, 
//	+ palette 16 entries, not compressed
//	+ 4 tiles (t|f|p reduced) not compressed
//	+ regular map (in SBBs), lz77 compressed, 160x64 
//	Total size: 32 + 128 + 2448 = 2608
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE2_H
#define GRIT_SURFACE2_H

#define surface2TilesLen 128
extern const unsigned int surface2Tiles[32];

#define surface2MapLen 2448
extern const unsigned short surface2Map[1224];

#define surface2PalLen 32
extern const unsigned short surface2Pal[16];

#endif // GRIT_SURFACE2_H

//}}BLOCK(surface2)
