
//{{BLOCK(surface4_1)

//======================================================================
//
//	surface4_1, 256x1024@4, 
//	+ palette 16 entries, not compressed
//	+ 140 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x128 
//	Total size: 32 + 4480 + 8192 = 12704
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE4_1_H
#define GRIT_SURFACE4_1_H

#define surface4_1TilesLen 4480
extern const unsigned int surface4_1Tiles[1120];

#define surface4_1MapLen 8192
extern const unsigned short surface4_1Map[4096];

#define surface4_1PalLen 32
extern const unsigned short surface4_1Pal[16];

#endif // GRIT_SURFACE4_1_H

//}}BLOCK(surface4_1)
