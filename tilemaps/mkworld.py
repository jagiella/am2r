#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 09:54:07 2019

@author: nick
"""

import json
import os
import cv2


#class C(object):
#    def __init__(self, keys, values):
#        for (key, value) in zip(keys, values):
#            self.__dict__[key] = value
#
#    def __setattr__(self, name, value):
#        raise Exception("It is read only!")
        
class Rectangle:
#    x = 0
#    y = 0
#    h = 0
#    w = 0
    def __init__(self, x,y,w,h):
        self.x=x
        self.y=y
        self.w=w
        self.h=h
        
    def __str__(self):
        return "Rectangle( x:%d, y:%d, w:%d, h:%d)" % (self.x,self.y,self.w,self.h)
    

def collision( rect1, rect2):
	if (rect1.x           <= rect2.x + rect2.w and
		rect1.x + rect1.w >= rect2.x and
		rect1.y           <= rect2.y + rect2.h and
		rect1.y + rect1.h >= rect2.y) :
		# collision detected!
		return True;
		
	return False;
	
def overlap(  r1, r2):
    x0 = max( r1.x,      r2.x)
    x1 = min( r1.x+r1.w, r2.x+r2.w)
    y0 = max( r1.y,      r2.y)
    y1 = min( r1.y+r1.h, r2.y+r2.h)
    
    return Rectangle(x0,y0, x1-x0, y1-y0)

def expandSmallDimension( r):
    if(r.w < r.h):
        r.w += 16
        r.x -= 8
    else:
        r.h += 16
        r.y -= 8
    return r
        
names = []
mapProperties = {}

print("parse world")
with open('world.world', 'r') as fp:
    data = json.load(fp)
    for map in data["maps"]:
        print( map["fileName"][:-4])
        
        img = cv2.imread(map["fileName"][:-4] + '.png', 0)
        #print( "grit surface1.png -gB4 -mRtpf -mLs -mzl -pn16 -ftc")
        print( img.shape)
        
        x = map["x"]
        y = map["y"]
        w = img.shape[1]
        h = img.shape[0]
#        print( )

        rect = Rectangle(x,y,w,h)
        print( rect)
        mapProperties[map["fileName"][:-4]] = rect
        names.append( map["fileName"][:-4])
        
#        os.system( "grit %s -gB4 -mRtpf -mLs -mzl -pn16 -ftc -fx ../tilesets/collisionTiles.png -gS -pS -O ../tilesets/collisionTiles" % (map["fileName"][:-4] + '.png'))
        os.system( "grit %s -gB4 -mLs -mzl -pn16 -ftc -fx ../tilesets/collisionTiles.png -gS -pS -O ../tilesets/collisionTiles" % (map["fileName"][:-4] + '.png'))
        os.system( "grit %s -gB4 -mRtpf -mLs -mz! -mp0 -pn16 -ftc" % (map["fileName"][:-4] + '-0.png'))
        os.system( "grit %s -gB4 -mRtpf -mLs -mz! -mp1 -pn16 -ftc" % (map["fileName"][:-4] + '-1.png'))
        
def tra2C( name, traList):
    s = "Transition %sTra[] = {\n" % (name)
    for tra in traList:
        r,i = tra
        s += "  { {{%d, %d}, {%d, %d}}, %d, 1, UP},\n" % (r.x, r.y, r.w, r.h, i)
    s += "};\n"
#    print( s)
    return s

with open("world.h", "w") as fp:
    
    print('room offsets')
    fp.write("int roomOffsets[%d][2] = {\n" % (len(names)))
    for name1 in names:
        rect1 = mapProperties[name1]
        fp.write("  {%d, %d},\n" % (rect1.x, rect1.y))
    fp.write("};\n")
    
    transitions = {}
    transitionLen = {}
    for i1,name1 in enumerate(names):
        rect1 = mapProperties[name1]
        transitions[ name1] = []
        for i2, name2 in enumerate(names):
            rect2 = mapProperties[name2]
            if( rect1 != rect2):
                if( collision(rect1, rect2)):
                    print("%s adjacent to %s" % (name1, name2))
                    r = expandSmallDimension( overlap( rect1, rect2))
                    r.x -= rect1.x
                    r.y -= rect1.y
                    print( r)
                    transitions[ name1].append( (r,i2))
        fp.write( tra2C( name1, transitions[ name1]))
        transitionLen[ name1] = len(transitions[ name1])
        
    
    s1 = "Transition *transitions[%d] = {" % len(transitionLen)
    s2 = "int transitionLen[%d] = {" % len(transitionLen)
    for name in names:
        length = transitionLen[name]
        
        s1 += "%sTra, " % (name)
        s2 += "%d, " % length
    s1 += "};\n"
    s2 += "};\n"
    print( s1)
    print( s2)
    fp.write( s1)
    fp.write( s2)
    
    print('Export map blocks')
    s = 'int mapBlocks[%d][2] = { ' % len(names)
    for name in names:
        s+= '{%d, %d},'%(mapProperties[name].w/(32*8), mapProperties[name].h/(32*8))
    s+='};\n'
    fp.write( s)
    
    print('Export backgrounds')
    s = 'const unsigned short* colMap[%d] = { ' % ( len(names))
    for name in names:
        s+= '%sMap,' % (name)
    s+='};\n'
    fp.write( s)
    s = 'int colMapLen[%d] = { ' % ( len(names))
    for name in names:
        s+= '%sMapLen,' % (name)
    s+='};\n'
    fp.write( s)


    print('Export backgrounds')
    for i in [0,1]:
        s = 'const unsigned int* bg%dTiles[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dTiles,' % (name, i)
        s+='};\n'
        fp.write( s)
        s = 'int bg%dTilesLen[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dTilesLen,' % (name, i)
        s+='};\n'
        fp.write( s)
        
        s = 'const unsigned short* bg%dPal[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dPal,' % (name, i)
        s+='};\n'
        fp.write( s)
        s = 'int bg%dPalLen[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dPalLen,' % (name, i)
        s+='};\n'
        fp.write( s)
        
        s = 'const unsigned short* bg%dMap[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dMap,' % (name, i)
        s+='};\n'
        fp.write( s)
        s = 'int bg%dMapLen[%d] = { ' % ( i, len(names))
        for name in names:
            s+= '%s_%dMapLen,' % (name, i)
        s+='};\n'
        fp.write( s)
#print( overlap( Rectangle(0,0, 2,2), Rectangle(1,1,2,2)))
#print(mapProperties)
#grit surface1-0.png -gB4 -mRtpf -mLs -mz! -mp0 -pn16 -ftc
#grit surface1-1.png -gB4 -mRtpf -mLs -mz! -mp1 -pn16 -ftc")