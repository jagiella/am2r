
//{{BLOCK(surface4)

//======================================================================
//
//	surface4, 256x1024@4, 
//	+ palette 16 entries, not compressed
//	+ 4 tiles (t|f|p reduced) not compressed
//	+ regular map (in SBBs), lz77 compressed, 32x128 
//	Total size: 32 + 128 + 988 = 1148
//
//	Time-stamp: 2020-02-04, 22:21:54
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE4_H
#define GRIT_SURFACE4_H

#define surface4TilesLen 128
extern const unsigned int surface4Tiles[32];

#define surface4MapLen 988
extern const unsigned short surface4Map[494];

#define surface4PalLen 32
extern const unsigned short surface4Pal[16];

#endif // GRIT_SURFACE4_H

//}}BLOCK(surface4)
