
//{{BLOCK(surface10_1)

//======================================================================
//
//	surface10_1, 1024x512@4, 
//	+ palette 16 entries, not compressed
//	+ 52 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 128x64 
//	Total size: 32 + 1664 + 16384 = 18080
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SURFACE10_1_H
#define GRIT_SURFACE10_1_H

#define surface10_1TilesLen 1664
extern const unsigned int surface10_1Tiles[416];

#define surface10_1MapLen 16384
extern const unsigned short surface10_1Map[8192];

#define surface10_1PalLen 32
extern const unsigned short surface10_1Pal[16];

#endif // GRIT_SURFACE10_1_H

//}}BLOCK(surface10_1)
