/*
 * HUD.cpp
 *
 *  Created on: 07.12.2019
 *      Author: nick
 */

// class definition
#include "HUD.h"

// associations
#include "Tilemap.h"

// sprites
#include "sprites/hud/numbersLarge.h"

void HUD::init( SpriteEngine *spriteEngine){
	//sTid = spriteEngine->addTiles( samus_running_sheetTiles, samus_running_sheetTilesLen);
	HUD::spriteEngine = spriteEngine;
	// health (numbers)
	sTid = spriteEngine->allocateTiles( 2*2 * 2);
	sPb  = spriteEngine->addPalette( numbersLargePal,   numbersLargePalLen);
	//spriteEngine->addTiles( samus_standing_sheetTiles, samus_standing_sheetTilesLen);
}

signed int HUD::sTid = 0;
signed int HUD::sPb = 0;
SpriteEngine *HUD::spriteEngine;

HUD::HUD( Player *player){
	_player = player;

	// init objects
	setObjectCount(2);

	obj_set_attr( obj(0),
				ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
				ATTR1_SIZE_16x16,              // 16x16
				ATTR2_PALBANK(sPb) | sTid | ATTR2_PRIO(0));
	obj_set_attr( obj(1),
				ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
				ATTR1_SIZE_16x16,              // 16x16
				ATTR2_PALBANK(sPb) | (sTid+2*2) | ATTR2_PRIO(0));
	obj_set_pos( obj(0), 0,  0);
	obj_set_pos( obj(1), 16, 0);
}

void HUD::updateScreen( Tilemap *tilemap){
	int digits[3];

	if( _player->health <= 99){
		digits[0] = 0;

		int rest = _player->health;
		digits[1] = rest / 10;
		rest = rest % 10;
		digits[2] = rest;
	}
//	digits[1] = 2;
//	digits[2] = 3;

	spriteEngine->copyTiles( digits[1]*2*2, sTid,     numbersLargeTiles, 2*2);
	spriteEngine->copyTiles( digits[2]*2*2, sTid+2*2, numbersLargeTiles, 2*2);

}
