/*
 * BackgroundHandler.h
 *
 *  Created on: 31.10.2019
 *      Author: nick
 */

#ifndef TILEMAPS_BACKGROUNDHANDLER_H_
#define TILEMAPS_BACKGROUNDHANDLER_H_

#ifndef DEBUG
#include "Tilemap.h"
#else
class Tilemap{
	int screenblockID;
public:
	Tilemap( const unsigned short *tilemap, int width, int height, int backgroundIndex, int screenblockIndex): screenblockID(screenblockIndex) {};
	int screenblockIndex() {return screenblockID;}
};
#endif

class Tileset;
class Tilemap;
class Palette;
class BackgroundHandler;

//#define surface2_0TilesLen 9760
//extern const unsigned int surface2_0Tiles[2440];
//
//#define surface2_0MapLen 20480
//extern const unsigned short surface2_0Map[10240];
//
//#define surface2_0PalLen 32
//extern const unsigned short surface2_0Pal[16];


class Palette{
public:
	Palette( const unsigned short *pal, const int len, int bankIndex);
//	copyToVRAM();
};

class Tileset{
public:
	unsigned int charblockIndex;
	Tileset( const unsigned int *tiles, const int len, int charblockIndex);
//	copyToVRAM();
};




class BackgroundHandler{
private:
	bool screenBlocks[32];
	bool backgroundIndices[4];
	volatile unsigned short* controlRegisters[4] = {
//			&REG_BG0CNT,
//			&REG_BG1CNT,
//			&REG_BG2CNT,
//			&REG_BG3CNT
	};
public:
	BackgroundHandler();
	void reset();
	int getFreeCharblockIndex( const int len);
	void allocateCharblock( int index, const int len);
	int getFreeScreenblockIndex( int requiredScreenblocks);
	void allocateScreenblocks( int index, const int len);
	int getFreeBackgroundIndex();
	Palette* addPalette( const unsigned short *pal, const int len, int paletteBankIndex); // need to be added in a certain order
	Tileset* addTileset( const unsigned int *tiles, const int len, int charblockIndex=-1);
	Tilemap* addBackground( const unsigned short *tilemap, int width, int height, Tileset* tiles, int priority=0, int screenblockIndex=-1, int backgroundIndex=-1);
	void copyToVRAM();
	unsigned int getControlFlags();
	void print();
};



#endif /* TILEMAPS_BACKGROUNDHANDLER_H_ */
