/*
 * Animation.h
 *
 *  Created on: 22.10.2019
 *      Author: nick
 */

#ifndef ANIMATION_H_
#define ANIMATION_H_


typedef struct {
	const unsigned int *tileSheet;
	const unsigned short *palette;
	int startTileIndex;
	int spriteSize;
	int animationFrames;
	bool loop;
	int VBlanksPerFrame;
}AnimationDefinition;

class Animation{
protected:
	SpriteEngine *spriteEngine;
	int targetTid;
	int targetPaletteBank;
	const AnimationDefinition *definition;
	//int x,y;
	int frameIndex;
	int VBlankCount;
	//bool
public:
	/*Animation( const unsigned int *tileSheet, int startTileIndex, int spriteSize, int animationFrames=1, bool loop=true, int VBlanksPerFrame):
		tileSheet(tileSheet), startTileIndex(startTileIndex), spriteSize(spriteSize), animationFrames(animationFrames), loop(loop), VBlanksPerFrame(VBlanksPerFrame)
	{

	}*/
	//Animation( AnimationDefinition *definition): definition(definition) {
	Animation( SpriteEngine *spriteEngine, int targetTid, int targetPaletteBank): spriteEngine(spriteEngine), targetTid(targetTid), targetPaletteBank(targetPaletteBank) {
		frameIndex = 0;
		VBlankCount = 0;
	};

	void setDefinition( const AnimationDefinition *def){
		definition = def;
		frameIndex = 0;
		VBlankCount = 0;
		//memcpy( &pal_obj_bank[targetPaletteBank], def->palette, 32);
		spriteEngine->copyPalette( targetPaletteBank, def->palette, 32);
	}
	void update(){
		if( VBlankCount % definition->VBlanksPerFrame == 0){
			// copy tile to VRAM
			spriteEngine->copyTiles( definition->startTileIndex + frameIndex*definition->spriteSize, targetTid, definition->tileSheet, definition->spriteSize);

			if( frameIndex + 1 < definition->animationFrames)
				frameIndex ++;
			else if( definition->loop)
				frameIndex = 0;
		}
		VBlankCount++;

	}

	int getFrameIndex(){
		return frameIndex;
	}
	bool isFinished(){
		return !definition->loop and frameIndex == definition->animationFrames-1;
	}
};


#endif /* ANIMATION_H_ */
