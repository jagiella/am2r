/*
 * Tilemap.cpp
 *
 *  Created on: 08.11.2019
 *      Author: nick
 */

#include "Tilemap.h"


int Tilemap::position2tile( int pos){
	//return floor(pos/8.);
	//return pos / 8;
	if(pos < 0)
		return pos / 8 - 1;
	else
		return pos / 8;
}

unsigned short Tilemap::getTileIndex( int x, int y){
	// get screen block id
	/*int mbc[2] = {x/(8*32), y/(8*32)};// map block coordinates
	int index[2] = {mbc[0]%2, mbc[1]%2};
	sbIDs[index[1]][index[0]];
	 */

	int tp[2] = {position2tile(x), position2tile(y)};

	// get map block id
	int mbc[2] = {tp[0]/32, tp[1]/32};// map block coordinates
	int mbID = mbc[0] + mbc[1]*map_w;

	// get tile id
	//int pc[2] = {x%(8*32), y%(8*32)}; // pixel coordinates inside map block
	int tc[2] = {tp[0]%32, tp[1]%32}; // tile coordinates inside map block
	int tID = tc[0] + tc[1]*32;

	return map[mbID*32*32 + tID];
	//return level1Map[mbID*32*32 + tID];
}



bool Tilemap::collisionDetection( int x0, int y0, int x1, int y1){
	for(int tx=position2tile(x0); tx<=position2tile(x1); tx++){
		if( tx>=0 and tx<map_w*32)
		{
			for(int ty=position2tile(y0); ty<=position2tile(y1); ty++){
				if( ty>=0 and ty<map_h*32)
				{
					int mbID = tx/32 + ty/32*map_w;
					int tc[2] = {tx%32, ty%32}; // tile coordinates inside map block
					int tID = tc[0] + tc[1]*32;

//						if( map[mbID*32*32 + tID] != map[0])
//						//if( level1Map[mbID*32*32 + tID] != 0)
//							return true;
//						//SE_ID_MASK
					if( (SE_ID_MASK & map[mbID*32*32 + tID]) == 1){
						return true;
					}

//						switch( SE_ID_MASK & map[mbID*32*32 + tID] ){
//						case 0: // empty == no collision
//							break;
//						case 1: // full  == collision
//							return true;
//						//case 2: // ascending right
//						default:
//							break;
//						}
				}
			}
		}
	}

	// test slopes

	int testPoints[][2] = {
			{x0, y1},
			{x1, y1},
	};

	for( int i=0; i<2; i++){
		SE screenentry = getTileIndex( testPoints[i][0], testPoints[i][1]);

		if( (screenentry == 3)){
			int localx = testPoints[i][0] % 8;
			int localy = testPoints[i][1] % 8;
			if( 8-localx <= localy )
				return true;
		}

		if( (screenentry == 2)){
			int localx = testPoints[i][0] % 8;
			int localy = testPoints[i][1] % 8;
			if( localx+1 <= localy )
				return true;
		}
	}


	return false;
}


