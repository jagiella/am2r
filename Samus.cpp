/*
 * Samus.cpp
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */


#include "SpriteEngine.h"
#include "Animation.h"

#include "Samus.h"

#include "sprites/samus/samus-running-sheet.h"
#include "sprites/samus/samus-standing-sheet.h"
#include "sprites/samus/jumping.h"
#include "sprites/samus/spinjump.h"
#include "sprites/samus/morphball.h"
#include "sprites/samus/gripping.h"

#define MAX(a,b) (a>b?a:b)

void Player::init( SpriteEngine *spriteEngine){
	//sTid = spriteEngine->addTiles( samus_running_sheetTiles, samus_running_sheetTilesLen);
	Player::spriteEngine = spriteEngine;
	sTid = spriteEngine->allocateTiles( 8*8);
	sPb  = spriteEngine->addPalette( samus_running_sheetPal,   samus_running_sheetPalLen);
	//spriteEngine->addTiles( samus_standing_sheetTiles, samus_standing_sheetTilesLen);
}

signed int Player::sTid = 0;
signed int Player::sPb = 0;
SpriteEngine *Player::spriteEngine;



Player::Player(){

	setObjectCount(1);

	obj_set_attr( obj(),
				ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
				ATTR1_SIZE_64x64,              // 64x64p,
				//ATTR1_SIZE_16x32,              // 64x64p,
				ATTR2_PALBANK(sPb) | sTid | ATTR2_PRIO(1));
				//obj_set_pos(sprite, x*16, y*16);
	obj_set_pos( obj(), x(), y());

	animation = new Animation(spriteEngine, sTid, sPb);
	setState( RUNNING);
	//animation->setDefinition( &animRunning);
};

bool Player::onGround( Tilemap* room){
	/*if( state == BALL)
		return room->collisionDetection( x+bb_ball[0][0], y+bb_ball[0][1]+1, x+bb_ball[1][0], y+bb_ball[1][1]+1);
	if( state == CROUCHING)
		return room->collisionDetection( x+bb_crouch[0][0], y+bb_crouch[0][1]+1, x+bb_crouch[1][0], y+bb_crouch[1][1]+1);
	 */

	return room->collisionDetection( x()+bb[0][0], y()+bb[0][1]+1, x()+bb[1][0], y()+bb[1][1]+1) or Collidable::CollisionBoxes->collision( x()+bb[0][0], y()+bb[0][1]+1, x()+bb[1][0], y()+bb[1][1]+1);
}

bool Player::collision( Tilemap* room){
	/*if( state == BALL)
		return room->collisionDetection( x+bb_ball[0][0], y+bb_ball[0][1], x+bb_ball[1][0], y+bb_ball[1][1]);
	if( state == CROUCHING)
		return room->collisionDetection( x+bb_crouch[0][0], y+bb_crouch[0][1], x+bb_crouch[1][0], y+bb_crouch[1][1]);
	*/

	return room->collisionDetection( x()+bb[0][0], y()+bb[0][1], x()+bb[1][0], y()+bb[1][1]) or Collidable::CollisionBoxes->collision( x()+bb[0][0], y()+bb[0][1], x()+bb[1][0], y()+bb[1][1]);
}

bool Player::collision( int x0, int y0, int x1, int y1){
	if (x0 < x()+bb[1][0] &&
	   x1 > x()+bb[0][0] &&
	   y0 < y()+bb[1][1] &&
	   y1 > y()+bb[0][1]) {
		// collision detected!
		return true;
	}
	return false;
}

const AnimationDefinition Player::animStanding  = { samus_standing_sheetTiles, samus_standing_sheetPal, 0,     8*8, 1, false, 1};
//const AnimationDefinition Player::animJumping   = { samus_standing_sheetTiles, samus_standing_sheetPal, 1*8*8, 8*8, 1, false, 1};
const AnimationDefinition Player::animJumping   = { jumpingTiles,              jumpingPal,              0*8*8, 8*8, 5, true, 4};
const AnimationDefinition Player::animSpinJump  = { spinjumpTiles,             spinjumpPal,             0*8*8, 8*8, 8, true, 2};
//const AnimationDefinition Player::animBall      = { samus_standing_sheetTiles, 3*8*8, 8*8, 1, false, 1};
const AnimationDefinition Player::animBall      = { morphballTiles,            morphballPal,            0*8*8, 8*8, 8, true, 4};
const AnimationDefinition Player::animCrouching = { samus_standing_sheetTiles, samus_standing_sheetPal, 2*8*8, 8*8, 1, false, 1};
const AnimationDefinition Player::animRunning   = { samus_running_sheetTiles,  samus_running_sheetPal,  0,     8*8, 10, true, 2};
const AnimationDefinition Player::animGripping  = { grippingTiles,  grippingPal,  0,         8*8, 1, false, 2};
const AnimationDefinition Player::animClimbing  = { grippingTiles,  grippingPal,  1*8*8,     8*8, 7, false, 4};

void Player::setState( int newState){
	//int bb[2][2]        = {{20,26}, {35,62}};//{42,62}};
	//int bb_crouch[2][2] = {{20,34}, {35,62}};//{42,62}};
	//int bb_ball[2][2]   = {{20,47}, {35,62}};//{42,62}};

	// update hitbox
	switch(newState){
	case BALL:
		bb[0][1] = 40;// 47 - 7 = 40
		break;
	case CROUCHING:
		bb[0][1] = 18;
		break;
	default:
		bb[0][1] = 15;// 47 - 31 = 16
		break;
	}

	switch(newState){
	case JUMPING:
//		state = JUMPING;
//		tileSheet = samus_standing_sheetTiles;
//		tid = 1*8*8;
		animation->setDefinition( &animJumping);

		jumpingCycles = 0;
		curSpeed[1] = -2.6;//= -4;
		break;

	case SPINJUMPING:
//		state = JUMPING;
//		tileSheet = samus_standing_sheetTiles;
//		tid = 1*8*8;
		animation->setDefinition( &animSpinJump);

		jumpingCycles = 0;
		//curSpeed[1] = -4.6;//= -4;
		curSpeed[1] = -2.6;//= -4;
		curSpeed[0] = 2*direction;
		break;

	case BALL:
//		state=BALL;
//		tileSheet = samus_standing_sheetTiles;
//		tid = 3*8*8;
		animation->setDefinition( &animBall);
		break;

	case STANDING:
//		state=STANDING;
//		tileSheet = samus_standing_sheetTiles;
//		tid = 0*8*8;
		animation->setDefinition( &animStanding);
		break;

	case CROUCHING:
//		state=CROUCHING;
//		tileSheet = samus_standing_sheetTiles;
//		tid = 2*8*8;
		curSpeed[0] = 0;
		animation->setDefinition( &animCrouching);
		break;

	case RUNNING:
//		state=RUNNING;
//		tileSheet = samus_running_sheetTiles;
//		tid = 0*8*8;
		animation->setDefinition( &animRunning);

		runningCycles=0;
		break;

	case GRIPPING:
		curSpeed[0] = 0;
		curSpeed[1] = 0;
		animation->setDefinition( &animGripping);
		break;

	case CLIMBING:

		animation->setDefinition( &animClimbing);
		break;
	}

	state = newState;
}

void Player::update( Tilemap *room){


	//objHandler->get( objID)->attr2= ATTR2_BUILD(sTid, sPb, 0);


	// PHYSICS

	float acceleration[2] = {0.,0.};
	float maxSpeed[2] = {2.,4.};

	bool wasOnGround = onGround(room);

	// horizontal movement
//	if( key_tri_horz() != 0){
//		if(state != CROUCHING and state!=SPINJUMPING){
//			acceleration[0] = key_tri_horz()*4;
//		}
//		direction = key_tri_horz();
//	}
//	else{
//		if( state!=SPINJUMPING){
//			acceleration[0] = -friction*SIGN(curSpeed[0]);
//			if( SIGN(curSpeed[0]) != SIGN(acceleration[0]+curSpeed[0])){
//				acceleration[0]=0;
//				curSpeed[0]=0;
//			}
//		}
//	}

	switch( state){
	case CLIMBING:
		if( animation->getFrameIndex() >= 4)
			curSpeed[0] = 	direction;
		break;
	case GRIPPING:
		// do nothing
//		if( key_tri_horz() == direction)
		if( key_hit(KEY_LEFT) and direction == -1 or key_hit(KEY_RIGHT) and direction == +1)
			setState( CLIMBING);
		else if( key_hit(KEY_A)){
			direction = -direction;
			setState( JUMPING);
		}
		break;
	case SPINJUMPING:
		if( key_tri_horz() != 0)
			direction = key_tri_horz();
		break;
	case CROUCHING:
		if( key_tri_horz() != 0)
			direction = key_tri_horz();
		break;
	default:
		// change direction
		if( key_tri_horz() != 0)
			direction = key_tri_horz();
		// accelerate
		acceleration[0] = key_tri_horz()*4 - friction*SIGN(curSpeed[0]);

	}
//	if( key_tri_horz() != 0){
//		if(state != CROUCHING and state!=SPINJUMPING){
//			acceleration[0] = key_tri_horz()*4;
//		}
//		direction = key_tri_horz();
//	}
//	else{
//		if( state!=SPINJUMPING){
//			acceleration[0] = -friction*SIGN(curSpeed[0]);
//			if( SIGN(curSpeed[0]) != SIGN(acceleration[0]+curSpeed[0])){
//				acceleration[0]=0;
//				curSpeed[0]=0;
//			}
//		}
//	}

	obj()->attr1 = ATTR1_SIZE_64x64 | (direction==-1?ATTR1_HFLIP:0);//ATTR1_BUILDR(0, ATTR1_SIZE_64x64, 0, 0);
	//sprite->attr1 = ATTR1_BUILDR( 0,64*64, (direction==-1)*3, 0);

		//curSpeed[0]=0;

	// vertical movement
	if( !wasOnGround){
		//targetSpeed[1] = 2;
		switch(state){
//		case CLIMBING:
//			break;
		case JUMPING:
			if(jumpingCycles < 23 and key_is_down(KEY_A)){
				acceleration[1] = 0;// delayed gravity
				jumpingCycles++;
			}else{
				jumpingCycles = 23;
				acceleration[1] = +0.51; // gravity
				//if( targetSpeed[1] > 0.5)
				//	targetSpeed[1] = 0.5;
			}
			break;

		case SPINJUMPING:
			jumpingCycles++;
			if( jumpingCycles >= 30 and key_hit(KEY_A)){
				// space jump
				setState(SPINJUMPING);
			}

			if(jumpingCycles < 15){// and key_is_down(KEY_A)){
				acceleration[1] = 0;// delayed gravity
				//jumpingCycles++;
			}
			else
				acceleration[1] = +0.21; // gravity
			break;

		case BALL:
			acceleration[1] = +0.51; // gravity
			break;

		case GRIPPING:
			// do nothing
			break;

		case CLIMBING:
			if( animation->getFrameIndex() < 3)
				curSpeed[1] = 	-2;
			if( animation->isFinished())
				setState(STANDING);
			break;

		default:
			// falling
			//state=JUMPING;
			//tileSheet = samus_standing_sheetTiles;
			//tid = 1*8*8;
			setState(JUMPING);
			jumpingCycles = 0;
			curSpeed[1] = 0;

		}
		runningCycles=0;
	}
	else{
		acceleration[1] = 0; // no gravity
		switch(state){
		case RUNNING:
			if( curSpeed[0] != 0){
				runningCycles ++;
				if(runningCycles % 2 == 0){
					tid = (tid + 8*8) % (10*8*8);
				}
				//tid = (tid + 8*8) % (10*8*8);

				if(key_hit(KEY_A)){
					// start jumping (spin)
					setState( SPINJUMPING);
					//animation->setDefinition( &animSpinJump);
				}
			}else{
				setState(STANDING);
			}
			//tid=240;
			break;

		case STANDING:
			if(key_tri_horz()!=0){
				// start running
				setState(RUNNING);
			}
			if(key_hit(KEY_A)){
				// start jumping (normal)
				setState( JUMPING);
			}
			if(key_hit(KEY_DOWN)){
				setState(CROUCHING);
				acceleration[0] = 0;
			}
			break;
		case CROUCHING:
			if(key_hit(KEY_DOWN)){
				setState(BALL);
			}
			if(key_hit(KEY_UP)){
				setState(STANDING);
			}
			if(key_hit(KEY_A)){
				// start jumping (normal)
				setState( JUMPING);
			}
			break;
		case BALL:
			if(key_hit(KEY_UP) and curSpeed[0] == 0){
				setState(CROUCHING);
//				acceleration[0] = 0;
			}
			break;
		case JUMPING:
		case SPINJUMPING:
			// stop jumping
			curSpeed[1] = 0;
			setState(STANDING);
			break;
		}
		/*if( (state==STANDING or state==CROUCHING) and key_tri_vert()==-1){
			// start jumping
			jumpingCycles = 23;
			curSpeed[1] = -2.6;//= -4;
			state = JUMPING;
		}else{
			// stop jumping
			curSpeed[1] = 0;
			if( key_tri_vert()==1){

			}
		}*/


		//runningCycles += fabs(curSpeed[0]/2.);
		//tid = ((int)(runningCycles) % 10)*(8*8);
	}
	curSpeed[0] = acceleration[0] + curSpeed[0];
	//curSpeed[1] = acceleration * targetSpeed[1] + (1-acceleration) * curSpeed[1];
	curSpeed[1] = acceleration[1] + curSpeed[1];
	if (fabs(curSpeed[0]) < threshold) curSpeed[0] = 0;
	if (fabs(curSpeed[1]) < threshold) curSpeed[1] = 0;

	if(fabs(curSpeed[0]) > maxSpeed[0]) curSpeed[0] = curSpeed[0] / fabs(curSpeed[0]) * maxSpeed[0];
	if(fabs(curSpeed[1]) > maxSpeed[1]) curSpeed[1] = curSpeed[1] / fabs(curSpeed[1]) * maxSpeed[1];


	// UPDATE POSITION

	int stepsx=abs( (int)position[0] - (int)(position[0]+curSpeed[0]) );
	int stepsy=abs( (int)position[1] - (int)(position[1]+curSpeed[1]) );
	int steps;
	if (stepsx > stepsy)
	   steps = stepsx;
	else
	   steps = stepsy;
	/*int steps;
	if (abs(curSpeed[0]) > abs(curSpeed[1]))
	   steps = abs(curSpeed[0]);
	else
	   steps = abs(curSpeed[1]);
	 */

	if( steps==0){
		// no collision detection needed
		position[0] += curSpeed[0];
		position[1] += curSpeed[1];

		//x = int(position[0]);
		//y = int(position[1]);
	}else{
	    // collision detection needed
		float Xincrement = curSpeed[0] / (float) steps;
		float Yincrement = curSpeed[1] / (float) steps;

		for(int v=0; v < steps; v++)
		{
			position[0] += Xincrement;

			if( int(position[0]) != int(position[0]-Xincrement)){
				if( collision(room)){
					position[1] --; // try moving up
					if( collision(room)){
						position[0] -= Xincrement;
						position[1] ++; // reverse upward movement
					}
				}else{
					// stick to floor
					if( wasOnGround and !onGround(room)){
						position[1] ++;
					}
				}
			}



			position[1] += Yincrement;

			if( int(position[1]) != int(position[1]-Yincrement)){
				if( collision(room)){
					position[1] -= Yincrement;
					curSpeed[1] = 0;
				}
			}

			if( Yincrement > 0){
				// grippling (left)
				if( //!onGround(room) and
						 //room->collisionDetection( x()+bb[0][0]-1, y()+bb[0][1],   x()+bb[1][0],   y()+bb[1][1]) and
						 room->collisionDetection( x()+bb[0][0]-1, y()+bb[0][1],   x()+bb[0][0]-1, y()+bb[0][1]) and
						!room->collisionDetection( x()+bb[0][0]-1, y()+bb[0][1]-1, x()+bb[0][0]-1, y()+bb[0][1]-1)){
					setState( GRIPPING);
					break;
				}
				// grippling (right)
				if( //!onGround(room) and
						 //room->collisionDetection( x()+bb[0][0],   y()+bb[0][1],   x()+bb[1][0]+1, y()+bb[1][1]) and
						 room->collisionDetection( x()+bb[1][0]+1, y()+bb[0][1],   x()+bb[1][0]+1, y()+bb[0][1]) and
						!room->collisionDetection( x()+bb[1][0]+1, y()+bb[0][1]-1, x()+bb[1][0]+1, y()+bb[0][1]-1)){
					setState( GRIPPING);
					break;
				}
			}

		}
	}
	/*position[0] += curSpeed[0];
	position[1] += curSpeed[1];

	x = int(position[0]);
	y = int(position[1]);
	 */

	if( invulnerableCycles > 0)
		invulnerableCycles --;
//	invulnerableCycles = MAX( 0, invulnerableCycles-1);

	// UPDATE ANIMATION
	animation->update();

	//spriteEngine->copyTiles( tid, sTid, tileSheet, 8*8);
}

void Player::updateScreen( Tilemap *room){
	obj_set_pos( obj(), x()-room->x, y()-room->y);
}

//int Player::objCount(){
//	return 1;
//}
//OBJ_ATTR Player::obj( int index){
//	return *ObjectHandler::instance().get( objID);
//}
