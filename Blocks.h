/*
 * blocks.h
 *
 *  Created on: 24.10.2019
 *      Author: nick
 */

#ifndef BLOCKS_H_
#define BLOCKS_H_

#include <stdlib.h>

#include "ObjectHandler.h"
#include "SpriteEngine.h"
#include "Samus.h"

#include "tilesets/blocks.h"

class BlockBase: public Object{
protected:
	static signed int sTid;
	static signed int sPb;
public:
	static void loadSprites( SpriteEngine *spriteEngine){
		sTid = spriteEngine->addTiles( blocksTiles, blocksTilesLen);
		sPb  = spriteEngine->addPalette( blocksPal,   blocksPalLen);
	}
	virtual ~BlockBase() {};
};

class FakeBlock: public BlockBase{
private:
//	static signed int sTid;
//	static signed int sPb;
	Player *_player;
	int _x, _y; // position
	int _horizontalBlockCount, _verticalBlockCount;
	int **blockIDs;
	int **desintegration;
public:
	FakeBlock( Player *player, int x, int y, int horizontalBlockCount=2, int verticalBlockCount=1): _player(player), _x(x), _y(y), _horizontalBlockCount(horizontalBlockCount), _verticalBlockCount(verticalBlockCount){
		blockIDs = (int**)malloc( _horizontalBlockCount*sizeof(int*));
		desintegration = (int**)malloc( _horizontalBlockCount*sizeof(int*));

		setObjectCount(_verticalBlockCount*_horizontalBlockCount);
		int blockID = 0;


		for(int i=0; i<_horizontalBlockCount; i++){
			blockIDs[i] = (int*)malloc( _verticalBlockCount*sizeof(int));
			desintegration[i] = (int*)malloc( _verticalBlockCount*sizeof(int));
			for(int j=0; j<_verticalBlockCount; j++){
				blockIDs[i][j] = blockID;
				obj_set_attr( obj(blockID),
							ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
							ATTR1_SIZE_16x16,              // 64x64p,
							ATTR2_PALBANK(BlockBase::sPb) | BlockBase::sTid);
				desintegration[i][j] = 10;
				blockID++;

			}
		}
//		Collidables::instance().add( this);
		setCollidable( new CollisionBox(this, x,y, horizontalBlockCount*16, verticalBlockCount*16));
	}

	~FakeBlock(){
//		ObjectHandler::instance().remove( objID);
	}

	void update( Tilemap *tilemap){
		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
//				obj_set_pos(ObjectHandler::instance().get( blockIDs[i][j]), _x+16*i-tilemap->x, _y+16*j-tilemap->y);

				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] > 0){
					// desintegration
					desintegration[i][j] --;
					if(desintegration[i][j]==0)
						desintegration[i][j] = -300;
				}
				if( !_player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] < 0 ){
					// integration
					desintegration[i][j] ++;
					if(desintegration[i][j]==0)
						desintegration[i][j] = +10;
				}
			}
		}
	};

	bool onScreen( int i, int j, Tilemap *tilemap){
		return _x+(i+1)*16 >= tilemap->x and _x+(i+1)*16 <= tilemap->x+240 and _y+(j+1)*16 >= tilemap->y and _y+j*16 <= tilemap->y+160;
	}

	void updateScreen( Tilemap *tilemap){
		//obj_set_pos(ObjectHandler::instance().get( objID), _x-tilemap->x, _y-tilemap->y);
		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
				obj_set_pos( obj( blockIDs[i][j]), _x+16*i-tilemap->x, _y+16*j-tilemap->y);

//				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] > 0){
//					// desintegration
//					desintegration[i][j] --;
//					if(desintegration[i][j]==0)
//						desintegration[i][j] = -300;
//				}
//				if( !_player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] < 0 ){
//					// integration
//					desintegration[i][j] ++;
//					if(desintegration[i][j]==0)
//						desintegration[i][j] = +10;
//				}

				if( desintegration[i][j]<0 or !onScreen( i, j, tilemap))
//				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) or !onScreen( i, j, tilemap))
					obj_hide( obj( blockIDs[i][j]));
				else
					obj_unhide( obj( blockIDs[i][j]), ATTR0_BLEND);

			}
		}
	}

	bool collision( int x0, int y0, int x1, int y1){

		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
				if( desintegration[i][j]>0 and
					x0 < _x+16*(1+i) &&
					x1 > _x+16*i &&
					y0 < _y+16*(1+j) &&
					y1 > _y+16*j)
					return true;
			}
		}
		/*if (x0 < _x+16*_horizontalBlockCount &&
		   x1 > _x &&
		   y0 < _y+16*_verticalBlockCount &&
		   y1 > _y) {
		    // collision detected!
			return true;
		}*/
		return false;
	}

//	void onCollision( Object *obj){
//		obj_hide( ObjectHandler::instance().get( objID));
//	}
};


class CrackedBlock: public BlockBase{
private:
//	static signed int sTid;
//	static signed int sPb;
	Player *_player;
	int _x, _y; // position
	int _horizontalBlockCount, _verticalBlockCount;
	int **blockIDs;
//	int **desintegration;

public:
//	static void loadSprites( SpriteEngine *spriteEngine){
//		sTid = spriteEngine->addTiles( blocksTiles, blocksTilesLen);
//		sPb  = spriteEngine->addPalette( blocksPal,   blocksPalLen);
//	}
	CrackedBlock( Player *player, int x, int y, int horizontalBlockCount=1, int verticalBlockCount=1): _player(player), _x(x), _y(y), _horizontalBlockCount(horizontalBlockCount), _verticalBlockCount(verticalBlockCount){

		setCollidable( new CollisionTilemap(this, 0,0, 16, 16, horizontalBlockCount, verticalBlockCount));


		blockIDs = (int**)malloc( _horizontalBlockCount*sizeof(int*));
//		desintegration = (int**)malloc( _horizontalBlockCount*sizeof(int*));

		setObjectCount(_verticalBlockCount*_horizontalBlockCount);
		int blockID = 0;

		for(int i=0; i<_horizontalBlockCount; i++){
			blockIDs[i] = (int*)malloc( _verticalBlockCount*sizeof(int));
//			desintegration[i] = (int*)malloc( _verticalBlockCount*sizeof(int));
			for(int j=0; j<_verticalBlockCount; j++){
				blockIDs[i][j] = blockID;
				obj_set_attr( obj( blockIDs[i][j]),
							ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
							ATTR1_SIZE_16x16,              // 64x64p,
							ATTR2_PALBANK(BlockBase::sPb) | (BlockBase::sTid+6*2*2));
//				desintegration[i][j] = 10;
				((CollisionTilemap*)collidable())->set(i,j, true);
				blockID ++;
			}
		}
//		Collidables::instance().add( this);

	}

	~CrackedBlock(){
		for(int i=0; i<_horizontalBlockCount; i++){
			free( blockIDs[i]);
//			free( desintegration[i]);
		}
		free( blockIDs);
//		free( desintegration);

//		Collidables::instance().remove( this);
	}
	int x(){
		return _x;
	}
	int y(){
		return _y;
	}
	void update( Tilemap *tilemap){
		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
//				obj_set_pos(ObjectHandler::instance().get( blockIDs[i][j]), _x+16*i-tilemap->x, _y+16*j-tilemap->y);

//				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] > 0){
//					// desintegration
//					desintegration[i][j] --;
//					if(desintegration[i][j]==0)
//						desintegration[i][j] = -300;
//				}
//				if( !_player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] < 0 ){
//					// integration
//					desintegration[i][j] ++;
//					if(desintegration[i][j]==0)
//						desintegration[i][j] = +10;
//				}
			}
		}
	};

	bool onScreen( int i, int j, Tilemap *tilemap){
		return _x+(i+1)*16 >= tilemap->x and _x+i*16 <= tilemap->x+240 and _y+(j+1)*16 >= tilemap->y and _y+j*16 <= tilemap->y+160;
	}

	void updateScreen( Tilemap *tilemap){
//		//obj_set_pos(ObjectHandler::instance().get( objID), _x-tilemap->x, _y-tilemap->y);
		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
				obj_set_pos( obj( blockIDs[i][j]), _x+16*i-tilemap->x, _y+16*j-tilemap->y);
//
////				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] > 0){
////					// desintegration
////					desintegration[i][j] --;
////					if(desintegration[i][j]==0)
////						desintegration[i][j] = -300;
////				}
////				if( !_player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) and desintegration[i][j] < 0 ){
////					// integration
////					desintegration[i][j] ++;
////					if(desintegration[i][j]==0)
////						desintegration[i][j] = +10;
////				}
//
				if( !((CollisionTilemap*)collidable())->get(i,j) or !onScreen( i, j, tilemap))
//				if( _player->collision( _x+i*16,_y+j*16-1, _x+(i+1)*16,_y+(j+1)*16-1) or !onScreen( i, j, tilemap))
					obj_hide( obj( blockIDs[i][j]));
				else
					obj_unhide( obj( blockIDs[i][j]), ATTR0_BLEND);
//
			}
		}
	}

	bool collision( int x0, int y0, int x1, int y1){

//		for(int i=0; i<_horizontalBlockCount; i++){
//			for(int j=0; j<_verticalBlockCount; j++){
//				if( desintegration[i][j]>0 and
//					x0 < _x+16*(1+i) &&
//					x1 > _x+16*i &&
//					y0 < _y+16*(1+j) &&
//					y1 > _y+16*j)
//					return true;
//			}
//		}
		return false;
	}

//	void onCollision( Object *obj){
//		obj_hide( ObjectHandler::instance().get( objID));
//	}

	void applyDamage( int x0, int y0, int x1, int y1){
		for(int i=0; i<_horizontalBlockCount; i++){
			for(int j=0; j<_verticalBlockCount; j++){
				if( x0 < _x+16*(i+1) &&
				   x1 > _x+16*i &&
				   y0 < _y+16*(j+1) &&
				   y1 > _y+16*j)
//				desintegration[i][j] = -1;
					((CollisionTilemap*)collidable())->set(i,j, false);
				//obj_hide( ObjectHandler::instance().get( blockIDs[i][j]));
			}
		}
	};
};

#endif /* BLOCKS_H_ */
