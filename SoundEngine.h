/*
 * SoundEngine.h
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#ifndef SOUNDENGINE_H_
#define SOUNDENGINE_H_

#include "tonc_types.h"

//#include "snd_01_AM2R_Title.h"


#define REG_SOUNDCNT_X *(u32*)0x04000084

#define SND_ENABLED    0x00000080
// register definitions

#define REG_SOUNDCNT_L        *(u16*)0x04000080

#define REG_SOUNDCNT_H        *(u16*)0x04000082



// flags

#define SND_ENABLED           0x00000080
#define SND_OUTPUT_RATIO_25   0x0000
#define SND_OUTPUT_RATIO_50   0x0001
#define SND_OUTPUT_RATIO_100  0x0002
#define DSA_OUTPUT_RATIO_50   0x0000
#define DSA_OUTPUT_RATIO_100  0x0004
#define DSA_OUTPUT_TO_RIGHT   0x0100
#define DSA_OUTPUT_TO_LEFT    0x0200
#define DSA_OUTPUT_TO_BOTH    0x0300
#define DSA_TIMER0            0x0000
#define DSA_TIMER1            0x0400
#define DSA_FIFO_RESET        0x0800
#define DSB_OUTPUT_RATIO_50   0x0000
#define DSB_OUTPUT_RATIO_100  0x0008
#define DSB_OUTPUT_TO_RIGHT   0x1000
#define DSB_OUTPUT_TO_LEFT    0x2000
#define DSB_OUTPUT_TO_BOTH    0x3000
#define DSB_TIMER0            0x0000
#define DSB_TIMER1            0x4000
#define DSB_FIFO_RESET        0x8000


// DMA channel 1 register definitions

#define REG_DMA1SAD         *(u32*)0x40000BC  // source address

#define REG_DMA1DAD         *(u32*)0x40000C0  // destination address

#define REG_DMA1CNT         *(u32*)0x40000C4  // control register



// DMA flags

#define WORD_DMA            0x04000000

#define HALF_WORD_DMA       0x00000000

#define ENABLE_DMA          0x80000000

#define START_ON_FIFO_EMPTY 0x30000000

#define DMA_REPEAT          0x02000000

#define DEST_REG_SAME       0x00400000



// Timer 0 register definitions

#define REG_TM0D            *(u16*)0x4000100

#define REG_TM0CNT          *(u16*)0x4000102



// Timer flags

#define TIMER_ENABLED       0x0080



// FIFO address defines

#define REG_FIFO_A          0x040000A0

#define REG_FIFO_B          0x040000A4



// our Timer interval that we calculated earlier (note that this

// value depends on our playback frequency and is therefore not set in

// stone)

#define TIMER_INTERVAL      (0xFFFF - 2097)


class SoundEngine{
	int VBlankCount;
	bool loop;

	const signed char *track;
	int trackLen;

public:
	bool running;

	SoundEngine(){
		REG_SOUNDCNT_X = SND_ENABLED;

		// we don't want to mess with sound channels 1-4
		REG_SOUNDCNT_L = 0;



		// enable and reset Direct Sound channel A, at full volume, using
		// Timer 0 to determine frequency
		REG_SOUNDCNT_H = SND_OUTPUT_RATIO_100 |
		                 DSA_OUTPUT_RATIO_100 |
		                 DSA_OUTPUT_TO_BOTH |
		                 DSA_TIMER0 |
		                 DSA_FIFO_RESET;

		// set the timer to overflow at the appropriate frequency and start it
		REG_TM0D   = TIMER_INTERVAL;
		REG_TM0CNT = TIMER_ENABLED;



		VBlankCount = 0;
		loop = false;
		running = false;
	}

	void setLoop( bool loop){
		this->loop = loop;
	}

	void startTrack( const signed char *track, const int trackLen){
		this->track    = track;
		this->trackLen = trackLen;

		// start the DMA transfer (assume that pSample is a (signed char*)
		// pointer to the buffer containing our sound data)
		REG_DMA1SAD = (u32)(this->track);
		REG_DMA1DAD = (u32)REG_FIFO_A;
		REG_DMA1CNT = ENABLE_DMA | START_ON_FIFO_EMPTY | WORD_DMA | DMA_REPEAT;

		VBlankCount = 0;
		running = true;
	}

	void stopTrack(){
		REG_DMA1CNT = 0;
		running = false;
	}

	void update(){
		VBlankCount ++; // 60Hz

		if( VBlankCount / 59.727 >= this->trackLen / 8000.){
			if( loop)
				this->startTrack( this->track, this->trackLen);
			else
				this->stopTrack();
		}
	}
};



#endif /* SOUNDENGINE_H_ */
