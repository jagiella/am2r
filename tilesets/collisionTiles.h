
//{{BLOCK(collisionTiles)

//======================================================================
//
//	collisionTiles, 8x32@4, 
//	+ palette 4 entries, not compressed
//	+ 4 tiles not compressed
//	Total size: 8 + 128 = 136
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_COLLISIONTILES_H
#define GRIT_COLLISIONTILES_H

#define collisionTilesTilesLen 128
extern const unsigned int collisionTilesTiles[32];

#define collisionTilesPalLen 8
extern const unsigned short collisionTilesPal[4];

#endif // GRIT_COLLISIONTILES_H

//}}BLOCK(collisionTiles)
