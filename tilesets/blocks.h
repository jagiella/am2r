
//{{BLOCK(blocks)

//======================================================================
//
//	blocks, 128x16@4, 
//	+ palette 16 entries, not compressed
//	+ 32 tiles Metatiled by 2x2 not compressed
//	Total size: 32 + 1024 = 1056
//
//	Time-stamp: 2019-11-08, 23:45:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BLOCKS_H
#define GRIT_BLOCKS_H

#define blocksTilesLen 1024
extern const unsigned int blocksTiles[256];

#define blocksPalLen 32
extern const unsigned short blocksPal[16];

#endif // GRIT_BLOCKS_H

//}}BLOCK(blocks)
