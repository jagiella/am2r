
//{{BLOCK(collisionTiles)

//======================================================================
//
//	collisionTiles, 8x32@4, 
//	+ palette 4 entries, not compressed
//	+ 4 tiles not compressed
//	Total size: 8 + 128 = 136
//
//	Time-stamp: 2019-11-29, 00:26:47
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.15
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

const unsigned int collisionTilesTiles[32] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,
	0x11111111,0x11111111,0x11111111,0x11111111,0x11111111,0x11111111,0x11111111,0x11111111,
	0x00000002,0x00000022,0x00000222,0x00002222,0x00022222,0x00222222,0x02222222,0x22222222,
	0x30000000,0x33000000,0x33300000,0x33330000,0x33333000,0x33333300,0x33333330,0x33333333,
};

const unsigned short collisionTilesPal[4] __attribute__((aligned(4))) __attribute__((visibility("hidden")))=
{
	0x0000,0x5E03,0x001F,0x03FF,
};

//}}BLOCK(collisionTiles)
