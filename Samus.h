/*
 * samus.h
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#ifndef SAMUS_H_
#define SAMUS_H_

#include <math.h>

extern "C" {
	#include "tonc_input.h"
	#include "tonc_oam.h"
}
#include "ObjectHandler.h"
#include "SpriteEngine.h"
#include "Animation.h"
#include "ObjectHandler.h"

#define SIGN(a) (a<0?-1:(a>0?+1:0))
#define TOINT(a) (a>=0 or a==(int)a ? (int)(a) : (int)(a-1))

class Player: public Object{

	// STATIC
private:
	static signed int sTid;
	static signed int sPb;
	static SpriteEngine *spriteEngine;
public:
	static void init( SpriteEngine *spriteEngine);

	// NON_STATIC
	static const AnimationDefinition animStanding;
	static const AnimationDefinition animCrouching;
	static const AnimationDefinition animBall;
	static const AnimationDefinition animJumping;
	static const AnimationDefinition animSpinJump;
	static const AnimationDefinition animRunning;
	static const AnimationDefinition animGripping;
	static const AnimationDefinition animClimbing;
	Animation *animation;


	const unsigned int *tileSheet;
	signed int tid = 0;
	signed int dtid = 2*4;//1*8*8;
	unsigned int pb  = 0;
	//OBJ_ATTR *sprite;
	int objID;
	//int _x=100,_y=16;
public:
	//int bb[2][2] = {{5,11}, {11,30}};
	int bb[2][2]        = {{24,26}, {39,47}};//{42,62}};
//	int bb_crouch[2][2] = {{20,34}, {35,62}};//{42,62}};
//	int bb_ball[2][2]   = {{20,47}, {35,62}};//{42,62}};

	float position[2] = {10., 16.};
	float curSpeed[2] = {0., 0.};
	float targetSpeed[2] = {0., 0.};
	float threshold = 0.01;
	float friction = 1; // 0...1
	int jumpingCycles = 0;
	int runningCycles = 0;
	int invulnerableCycles = 0;
public:
	int direction = 0;

	enum STATE {STANDING, CROUCHING, BALL, RUNNING, JUMPING, SPINJUMPING, GRIPPING, CLIMBING};
	int state = STATE::BALL;
	int health = 99;
public:
	Player();
	bool onGround( Tilemap* room);
	bool collision( Tilemap* room);
	bool collision( int x0, int y0, int x1, int y1);
	void setState( int state);
	void update( Tilemap *room);
	void updateScreen( Tilemap *room);
	int x(){ return TOINT(position[0]);}
	int y(){ return TOINT(position[1]);}

	// Object methods
//	int objCount();
//	OBJ_ATTR obj( int index);
};




#endif /* SAMUS_H_ */
