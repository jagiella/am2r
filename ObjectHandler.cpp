/*
 * ObjectHandler.cpp
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */


#include "ObjectHandler.h"
#include "Collision.h"

ObjectHandler *ObjectHandler::pInstance;
//Collidables *Collidables::pInstance;


// OBJECT

Object::Object() {
	_objCount = 0;
	_objects = 0;
	_collidable = 0;
	_eventBox = 0;
	_suspended = false;
}

Object::~Object() {
	setObjectCount(0);
	if( _collidable)
		delete _collidable;
	if( _eventBox)
		delete _eventBox;
}

void Object::setObjectCount( int count) {
	if( _objCount != 0)
		free( _objects);

	if( count != 0)
		_objects = (OBJ_ATTR*) malloc( count * sizeof(OBJ_ATTR));
	_objCount = count;
}


int Object::objCount() {
	return _objCount;
};

OBJ_ATTR *Object::obj( int index) {
	return &_objects[ index];
};

void Object::setCollidable( Collidable* collidable) {
	_collidable = collidable;
	collidable->setList(Collidable::CollisionBoxes);
};
void Object::setEventBox( Collidable* collidable) {
	_eventBox = collidable;
	collidable->setList(Collidable::EventBoxes);
};
void Object::setHitBox( Collidable* collidable) {
	_hitBox = collidable;
	collidable->setList(Collidable::HitBoxes);
};




// COLLIDABLE
Collidables *Collidable::CollisionBoxes = new Collidables();
Collidables *Collidable::EventBoxes = new Collidables();
Collidables *Collidable::HitBoxes = new Collidables();


Collidable::Collidable( Object *object, Collidables *list): _object(object), _list(list) {
	if( _list)
		_list->add( this);
};

Collidable::~Collidable() {
	if( _list)
		_list->remove( this);
};

Object *Collidable::object() {
	return _object;
}

void Collidable::setList( Collidables *list){
	_list = list;
	_list->add( this);
}

bool Collidable::collision( int x0, int y0, int x1, int y1) {
//	return _object->collision( x0, y0, x1, y1);
	return false;
}

void Collidable::applyDamage( int x0, int y0, int x1, int y1){
	_object->applyDamage( x0, y0, x1, y1);

};


// COLLISION BOX

CollisionBox::CollisionBox( Object *object, int x, int y, int w, int h) : Collidable(object), x(x), y(y), w(w), h(h) {
//	List->add( this);
};

CollisionBox::~CollisionBox() {
//	List->remove( this);
};

bool CollisionBox::collision( int x0, int y0, int x1, int y1){
	return testRectangleCollision(
			x0, y0, x1, y1,
			_object->x()+x,   _object->y()+y,
			_object->x()+x+w, _object->y()+y+h
			);
}


// COLLISION Tilemap

CollisionTilemap::CollisionTilemap( Object *object, int x, int y, int w, int h, int nx, int ny) : Collidable(object), x(x), y(y), w(w), h(h), nx(nx), ny(ny) {
	this->tilemap = (bool**)malloc( nx*sizeof(bool*));
	for(int i=0; i<nx; i++){
		this->tilemap[i] = (bool*)malloc( ny*sizeof(bool));
		for(int j=0; j<ny; j++){
			this->tilemap[i][j] = true;
		}
	}

//	List->add( this);
};

CollisionTilemap::~CollisionTilemap() {
	for(int i=0; i<nx; i++){
		free( this->tilemap[i]);
	}
	free( this->tilemap);
//	List->remove( this);
};

bool CollisionTilemap::collision( int x0, int y0, int x1, int y1){
	for(int i=0; i<nx; i++){
		for(int j=0; j<ny; j++){
			if( tilemap[i][j] and
				x0 < _object->x()+x+w*(1+i) &&
				x1 > _object->x()+x+w*i &&
				y0 < _object->y()+y+h*(1+j) &&
				y1 > _object->y()+y+h*j)
				return true;
		}
	}
	return false;
}

void CollisionTilemap::set( int i, int j, bool value){
	tilemap[i][j] = value;
}
bool CollisionTilemap::get( int i, int j){
	return tilemap[i][j];
}


// COLLISION POLYGON

CollisionPolygon::CollisionPolygon( Object *object, int polygon[][2], int length) : Collidable(object), length(length) {
	this->polygon = (int**) malloc( length*sizeof(int*));
	for( int i=0; i<length; i++){
		this->polygon[i] = (int*) malloc( 2*sizeof(int));
		this->polygon[i][0] = polygon[i][0];
		this->polygon[i][1] = polygon[i][1];
	}
//	List->add( this);
};

CollisionPolygon::~CollisionPolygon() {
	for( int i=0; i<length; i++){
		free( this->polygon[i]);
	}
	free( this->polygon);
//	List->remove( this);
};

bool CollisionPolygon::collision( int x0, int y0, int x1, int y1){
	if( testRectangleCollision(x0,y0,x1,y1, _object->x(),_object->y(),_object->x()+192,_object->y()+64)){
		 int Q[4][2] = {
				 {x0-_object->x(), y0-_object->y()},
				 {x0-_object->x(), y1-_object->y()},
				 {x1-_object->x(), y0-_object->y()},
				 {x1-_object->x(), y1-_object->y()}
		 };
		 for( int j=0; j<4; j++){
			 if( testPointInPolygon(Q[j], polygon, 6) >= 0)
				 return true;
		 }
	 }
	 return false;
}
