/*
 * Collision.c
 *
 *  Created on: 06.11.2019
 *      Author: nick
 */

//#include <math.h>

#include "Collision.h"

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

int KreuzProdTest( int *A, int *B, int C[2]){
	int *a = A, *b, *c;
	if( B[1] > C[1]){
		b = C; c = B;
	}else{
		b = B; c = C;
	}

	if( a[1] <= b[1] or a[1] > c[1]){
		return +1;
	}

	int delta = (b[0]-a[0]) * (c[1]-a[1])
						   - (b[1]-a[1]) * (c[0]-a[0]);
	if( delta > 0)
		return +1; // inside
	if( delta < 0)
		return -1; // outside
	else
		return  0; // on edge
}

int testPointInPolygon( int *p, int **polygon, int polygonLen){
	int t = -1;
	for( int i=0; i<polygonLen-1; i++){
		t *= KreuzProdTest( p, polygon[i], polygon[i+1]);
		if( t == 0)
			break;
	}
	return t;
}






// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
bool onSegment(Point p, Point q, Point r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
        q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
       return true;

    return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(Point p, Point q, Point r)
{
    // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
    // for details of below formula.
    int val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);

    if (val == 0) return 0;  // colinear

    return (val > 0)? 1: 2; // clock or counterclock wise
}

// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
bool doIntersect(Point p1, Point q1, Point p2, Point q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    // General case
    if (o1 != o2 && o3 != o4)
        return true;

    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;

    // p1, q1 and q2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;

    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;

     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;

    return false; // Doesn't fall in any of the above cases
}


bool testRectangleCollision( int rect1_x0, int rect1_y0, int rect1_x1, int rect1_y1, int rect2_x0, int rect2_y0, int rect2_x1, int rect2_y1){
	if ( rect1_x0 < rect2_x1 &&
		 rect1_x1 > rect2_x0 &&
		 rect1_y0 < rect2_y1 &&
		 rect1_y1 > rect2_y0) {
		// collision detected!
		return true;
	}
	return false;
}
