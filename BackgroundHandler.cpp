/*
 * BackgroundHandler.cpp
 *
 *  Created on: 31.10.2019
 *      Author: nick
 */

//#include "tonc_memmap.h"

#include "BackgroundHandler.h"

#include <string.h>
#include <stdio.h>

Palette::Palette( const unsigned short *pal, const int len, int bankIndex){
#ifndef DEBUG
	memcpy32(pal_bg_bank[bankIndex], pal, len/4);
//	memcpy(pal_bg_bank[bankIndex], pal, len);
#endif
}


Tileset::Tileset( const unsigned int *tiles, const int len, int charblockIndex): charblockIndex(charblockIndex){
//	charblockIndex = handler->getFreeCharblockIndex( len);
	this->charblockIndex = charblockIndex;
#ifndef DEBUG
	memcpy32(tile_mem[charblockIndex], tiles, len/4);
//	memcpy(tile_mem[charblockIndex], tiles, len);
#endif
}


BackgroundHandler::BackgroundHandler(){
	reset();
};


void BackgroundHandler::reset(){
	for( int i=0; i<32; i++)
		screenBlocks[i] = false;
	for( int i=0; i<4; i++)
		backgroundIndices[i] = false;

};

int BackgroundHandler::getFreeCharblockIndex( const int len){

	int requiredScreenblocks = (len+2047) / 2048; // round-up
	for( int i=0; i<4; i++){ // 4 charblocks
		int j=0; // free screenblocks
		for( ; j<requiredScreenblocks and i*8+j < 32 and screenBlocks[i*8+j] == false; j++);
		if( j == requiredScreenblocks){
//			for( int k=0; k<requiredScreenblocks; k++)
//				screenBlocks[i*8+k] = true;
			allocateCharblock( i, len);
			return i;
		}
	}

	return 0; // should never happen!!!
};

void BackgroundHandler::allocateCharblock( int index, const int len){
	int requiredScreenblocks = (len+2047) / 2048; // round-up
	for( int k=0; k<requiredScreenblocks; k++)
		screenBlocks[index*8+k] = true;
}

int BackgroundHandler::getFreeScreenblockIndex( int requiredScreenblocks){

	//int requiredScreenblocks = (len+2047) / 2048/2; // round-up
	int countFreeScreenblocks = 0;
	for( int i=0; i<32; i++){
		if( screenBlocks[i] == false){
			countFreeScreenblocks++;
			if( countFreeScreenblocks == requiredScreenblocks){
				int screenblockIndex = i+1-countFreeScreenblocks;
				for( int j=screenblockIndex; j<screenblockIndex+requiredScreenblocks; j++)
					screenBlocks[j] = true;
				return screenblockIndex;
			}
		}
		else
			countFreeScreenblocks=0;
	}

	return 0; // should never happen!!!
};

void BackgroundHandler::allocateScreenblocks( int index, const int len){
	int requiredScreenblocks = (len+2047) / 2048; // round-up
	for( int k=0; k<requiredScreenblocks; k++)
		screenBlocks[index+k] = true;
}


int BackgroundHandler::getFreeBackgroundIndex(){
	for( int i=0; i<4; i++)
		if( backgroundIndices[i] == false){
			backgroundIndices[i] = true;
			return i;
		}
	return 0; // should never happen!!!
}

Palette* BackgroundHandler::addPalette( const unsigned short *pal, const int len, int bankIndex){
	return new Palette( pal, len, bankIndex);
}; // need to be added in a certain order


Tileset* BackgroundHandler::addTileset( const unsigned int *tiles, const int len, int charblockIndex){
	if( charblockIndex == -1)
		// find and allocate screenblocks
		charblockIndex = this->getFreeCharblockIndex( len);
	else{
		// allocate screenblocks
		allocateCharblock( charblockIndex, len);
	}
	return new Tileset(tiles, len, charblockIndex);
};


Tilemap* BackgroundHandler::addBackground( const unsigned short *tilemap, int width, int height, Tileset* tiles, int priority, int screenblockIndex, int backgroundIndex){
	if(backgroundIndex == -1){
		backgroundIndex = getFreeBackgroundIndex();
	}else
		backgroundIndices[ backgroundIndex] = true;

	if( screenblockIndex == -1)
		// find and allocate screenblocks
		screenblockIndex = getFreeScreenblockIndex(4);
	else
		// allocate screenblocks
		allocateScreenblocks( screenblockIndex, 2048*2*2);

	Tilemap *tmap = new Tilemap(tilemap, width, height, backgroundIndex, screenblockIndex);

#ifndef DEBUG
	switch(backgroundIndex){
	case 0:
		REG_BG0CNT = BG_CBB(tiles->charblockIndex) | BG_SBB(tmap->screenblockIndex()) | BG_4BPP | BG_REG_64x64 | BG_PRIO(priority);
		break;
	case 1:
		REG_BG1CNT = BG_CBB(tiles->charblockIndex) | BG_SBB(tmap->screenblockIndex()) | BG_4BPP | BG_REG_64x64 | BG_PRIO(priority);
		break;
	case 2:
		REG_BG2CNT = BG_CBB(tiles->charblockIndex) | BG_SBB(tmap->screenblockIndex()) | BG_4BPP | BG_REG_64x64 | BG_PRIO(priority);
		break;
	case 3:
		REG_BG3CNT = BG_CBB(tiles->charblockIndex) | BG_SBB(tmap->screenblockIndex()) | BG_4BPP | BG_REG_64x64 | BG_PRIO(priority);
		break;
	}
#endif
	//*controlRegisters[backgroundIndex] = BG_CBB(tiles->charblockIndex) | BG_SBB(tmap->screenblockIndex()) | BG_4BPP | BG_REG_64x64 | BG_PRIO(priority);

#ifdef DEBUG
	printf("REG_BG%dCNT = BG_CBB(%d) | BG_SBB(%d)\n", backgroundIndex, tiles->charblockIndex, tmap->screenblockIndex());
#endif

	return tmap;
};


void BackgroundHandler::copyToVRAM(){


};

unsigned int BackgroundHandler::getControlFlags(){
	unsigned int flags = 0;
#ifndef DEBUG
	if( backgroundIndices[0])
			flags |= DCNT_BG0;
	if( backgroundIndices[1])
			flags |= DCNT_BG1;
	if( backgroundIndices[2])
			flags |= DCNT_BG2;
	if( backgroundIndices[3])
			flags |= DCNT_BG3;
#endif
	return flags;
}

void BackgroundHandler::print(){
	for( int i=0; i<32; i++)
		printf("%d", screenBlocks[i]);
	printf("\n");
}
